/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.requests;

import java.util.UUID;

import network.stratus.api.bukkit.models.punishments.BukkitPunishmentFactory;
import network.stratus.api.client.APIClient;
import network.stratus.api.client.APIRequest;
import network.stratus.api.client.APIResponse;
import network.stratus.api.models.punishment.PunishmentFactory.Type;

/**
 * Represents a request to issue a {@link Punishment} against a
 * {@link org.bukkit.entity.Player}.
 * 
 * @author Ian Ballingall
 *
 */
public class PunishmentCreateRequest implements APIRequest<BukkitPunishmentFactory> {

	private UUID issuer;
	private String issuerOfflineName;
	private String target;
	private String reason;
	private Type type;
	private String duration;

	public PunishmentCreateRequest(UUID issuer, String issuerOfflineName, String target, String reason, Type type) {
		this.issuer = issuer;
		this.issuerOfflineName = issuerOfflineName;
		this.target = target;
		this.reason = reason;
		this.type = type;
	}

	public PunishmentCreateRequest(UUID issuer, String issuerOfflineName, String target, String reason, String duration,
			Type type) {
		if (type != Type.BAN && type != Type.MUTE) {
			throw new IllegalArgumentException("Cannot set duration on type: " + type);
		}

		this.issuer = issuer;
		this.issuerOfflineName = issuerOfflineName;
		this.target = target;
		this.reason = reason;
		this.type = type;
		this.duration = duration;
	}

	public UUID getIssuer() {
		return issuer;
	}

	public String getIssuerOfflineName() {
		return issuerOfflineName;
	}

	public String getTarget() {
		return target;
	}

	public String getReason() {
		return reason;
	}

	public Type getType() {
		return type;
	}

	public String getDuration() {
		return duration;
	}

	@Override
	public String getEndpoint() {
		return "/punishments/create";
	}

	@Override
	public Class<BukkitPunishmentFactory> getResponseType() {
		return BukkitPunishmentFactory.class;
	}

	@Override
	public APIResponse<BukkitPunishmentFactory> makeRequest(APIClient client) {
		return client.post(this);
	}

}

/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.requests.teams;

import java.util.UUID;

import javax.annotation.Nullable;

import network.stratus.api.bukkit.responses.teams.TeamRosterResponse;
import network.stratus.api.client.APIClient;
import network.stratus.api.client.APIRequest;
import network.stratus.api.client.APIResponse;

/**
 * Represents a request to view a team's roster.
 * 
 * @author Ian Ballingall
 *
 */
public class TeamRosterRequest implements APIRequest<TeamRosterResponse> {

	private @Nullable UUID sender;
	private String teamName;

	public TeamRosterRequest(@Nullable UUID sender, String teamName) {
		this.sender = sender;
		this.teamName = teamName;
	}

	public String getTeamName() {
		return teamName;
	}

	@Nullable
	public UUID getSender() {
		return sender;
	}

	@Override
	public String getEndpoint() {
		if (teamName.isEmpty()) {
			if (sender == null) {
				throw new IllegalStateException("Either team name or sender must be defined");
			} else {
				return "/teams/roster/player/" + sender.toString();
			}
		} else {
			return "/teams/roster/name/" + teamName.replace("/", "%2f");
		}
	}

	@Override
	public Class<TeamRosterResponse> getResponseType() {
		return TeamRosterResponse.class;
	}

	@Override
	public APIResponse<TeamRosterResponse> makeRequest(APIClient client) {
		return client.get(this);
	}

}

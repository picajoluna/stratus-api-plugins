/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.requests;

import network.stratus.api.bukkit.responses.ReportListResponse;
import network.stratus.api.client.APIClient;
import network.stratus.api.client.APIRequest;
import network.stratus.api.client.APIResponse;

/**
 * Represents a request to the API for a list of
 * {@link network.stratus.api.bukkit.models.Report}s.
 * 
 * @author Ian Ballingall
 *
 */
public class ReportListRequest implements APIRequest<ReportListResponse> {

	/** Whether reports from all servers should be shown. */
	private boolean allServers;
	/** The server whose reports to view. */
	private String targetServer;
	/** The player whose reports to view. */
	private String targetPlayer;
	/** The page number. */
	private int page;

	public ReportListRequest(boolean allServers, String targetServer, String targetPlayer, int page) {
		this.allServers = allServers;
		this.targetServer = targetServer;
		this.targetPlayer = targetPlayer;
		this.page = page;
	}

	public boolean isAllServers() {
		return allServers;
	}

	public String getTargetServer() {
		return targetServer;
	}

	public String getTargetPlayer() {
		return targetPlayer;
	}

	public int getPage() {
		return page;
	}

	@Override
	public String getEndpoint() {
		return "/reports/list";
	}

	@Override
	public Class<ReportListResponse> getResponseType() {
		return ReportListResponse.class;
	}

	@Override
	public APIResponse<ReportListResponse> makeRequest(APIClient client) {
		return client.post(this);
	}

}

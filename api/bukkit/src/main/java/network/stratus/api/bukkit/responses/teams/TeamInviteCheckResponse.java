/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.responses.teams;

import java.util.List;

import network.stratus.api.bukkit.util.Util;

/**
 * Represents a response to a check for team invites.
 * 
 * @author Ian Ballingall
 *
 */
public class TeamInviteCheckResponse {

	private List<TeamInviteResponse> invites;

	public List<TeamInviteResponse> getInvites() {
		return invites;
	}

	/**
	 * Sends the invites in this response to the player, if they are online.
	 */
	public void listInvites() {
		for (TeamInviteResponse invite : invites) {
			Util.sendTeamInviteMessage(invite.getTarget(), invite.getTeam());
		}
	}

}

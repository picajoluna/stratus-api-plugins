/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.requests.teams;

import java.util.UUID;

import network.stratus.api.bukkit.responses.teams.TeamLeaderPromoteResponse;
import network.stratus.api.client.APIClient;
import network.stratus.api.client.APIRequest;
import network.stratus.api.client.APIResponse;

/**
 * Represents a request to change the team leader.
 * 
 * @author Ian Ballingall
 *
 */
public class TeamLeaderPromoteRequest implements APIRequest<TeamLeaderPromoteResponse> {

	private UUID sender;
	private String newLeader;

	public TeamLeaderPromoteRequest(UUID sender, String newLeader) {
		this.sender = sender;
		this.newLeader = newLeader;
	}

	public UUID getSender() {
		return sender;
	}

	public String getNewLeader() {
		return newLeader;
	}

	@Override
	public String getEndpoint() {
		return "/teams/promote";
	}

	@Override
	public Class<TeamLeaderPromoteResponse> getResponseType() {
		return TeamLeaderPromoteResponse.class;
	}

	@Override
	public APIResponse<TeamLeaderPromoteResponse> makeRequest(APIClient client) {
		return client.post(this);
	}

}

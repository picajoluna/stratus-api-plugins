/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.requests.teams;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnore;

import network.stratus.api.bukkit.responses.teams.TeamInviteResponse;
import network.stratus.api.client.APIClient;
import network.stratus.api.client.APIRequest;
import network.stratus.api.client.APIResponse;

/**
 * Represents a request to invite someone to a team.
 * 
 * @author Ian Ballingall
 *
 */
public class TeamInviteRequest implements APIRequest<TeamInviteResponse> {

	private UUID sender;
	private String target;
	@JsonIgnore
	private boolean cancel;

	public TeamInviteRequest(UUID sender, String target, boolean cancel) {
		this.sender = sender;
		this.target = target;
		this.cancel = cancel;
	}

	public UUID getSender() {
		return sender;
	}

	public String getTarget() {
		return target;
	}

	@JsonIgnore
	public boolean getCancel() {
		return cancel;
	}

	@Override
	public String getEndpoint() {
		return cancel ? "/teams/invite/" + sender + "/" + target : "/teams/invite";
	}

	@Override
	public Class<TeamInviteResponse> getResponseType() {
		return TeamInviteResponse.class;
	}

	@Override
	public APIResponse<TeamInviteResponse> makeRequest(APIClient client) {
		return cancel ? client.delete(this) : client.post(this);
	}

}

/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.requests;

import network.stratus.api.bukkit.responses.FindPlayerResponse;
import network.stratus.api.client.APIClient;
import network.stratus.api.client.APIRequest;
import network.stratus.api.client.APIResponse;

/**
 * Represents a request to the API to find a player's current server, or when
 * and where they were last seen online.
 * 
 * @author Ian Ballingall
 *
 */
public class FindPlayerRequest implements APIRequest<FindPlayerResponse> {

	/** The target username. */
	private String target;

	public FindPlayerRequest(String target) {
		this.target = target;
	}

	public String getTarget() {
		return target;
	}

	@Override
	public String getEndpoint() {
		return "/players/find";
	}

	@Override
	public Class<FindPlayerResponse> getResponseType() {
		return FindPlayerResponse.class;
	}

	@Override
	public APIResponse<FindPlayerResponse> makeRequest(APIClient client) {
		return client.post(this);
	}

}

/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.models.punishments;

import java.util.Date;
import java.util.UUID;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.MultiAudience;
import network.stratus.api.models.punishment.PunishmentFactory.Type;

/**
 * Represents a kick.
 * 
 * @author Ian Ballingall
 *
 */
public class Kick extends BukkitPunishment {

	public Kick(String _id, UUID issuer, String issuerOfflineName, UUID target, String targetOfflineName, Date time,
			Date expiry, String reason, boolean active, int number, String serverName) {
		super(_id, issuer, issuerOfflineName, target, targetOfflineName, time, expiry, reason, active, number,
				serverName);
	}

	@Override
	public void enforce(boolean showServer) {
		initialisePlayers();
		formatNames();

		MultiAudience everyone = new MultiAudience.Builder().global().build();

		if (showServer) {
			everyone.sendMessage("punishment.kick.broadcast.server", serverName,
					(issuerPlayer == null) ? issuerOfflineName : issuerPlayer.getDisplayName(),
					(targetPlayer == null) ? targetOfflineName : targetPlayer.getDisplayName(), reason);
		} else {
			everyone.sendMessage("punishment.kick.broadcast",
					(issuerPlayer == null) ? issuerOfflineName : issuerPlayer.getDisplayName(),
					(targetPlayer == null) ? targetOfflineName : targetPlayer.getDisplayName(), reason);
		}

		if (targetPlayer != null) {
			String kickMessage = StratusAPI.get().getTranslator().getStringOrDefaultLocale(targetPlayer.getLocale(),
					"punishment.kick.message");
			targetPlayer.kickPlayer(String.format(kickMessage, reason));
		}
	}

	@Override
	public Type getType() {
		return Type.KICK;
	}

}

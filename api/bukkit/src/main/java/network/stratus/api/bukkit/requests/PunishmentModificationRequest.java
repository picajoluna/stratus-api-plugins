/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.requests;

import java.util.UUID;

import network.stratus.api.client.APIClient;
import network.stratus.api.client.APIRequest;
import network.stratus.api.client.APIResponse;

/**
 * Represents a request to modify a
 * {@link me.iangbb.stratus.api.bukkit.models.Punishment}.
 * 
 * @author Ian Ballingall
 *
 */
public class PunishmentModificationRequest implements APIRequest<Void> {

	/** The UUID of the request issuer. */
	private UUID issuer;
	/** The username of the target player. */
	private String target;
	/** The punishment number to modify; this is a counter for each player. */
	private int number;
	/**
	 * Whether this user is permitted to modify punishments issued by other people.
	 */
	private boolean canModifyOthers;
	/** The action being taken. */
	private Action action;
	/** Associated data, such as the new duration or new reason. */
	private String data;

	/**
	 * Represents the action being taken in this request.
	 */
	public enum Action {
		/** Appeal the given punishment - mark as invalid. */
		APPEAL("APPEAL"),
		/** Set the expiry of the given punishment. */
		EXPIRE("EXPIRE"),
		/** Change the reason of the given punishment. */
		REASON("REASON"),
		/** Set the type of the given punishment. */
		TYPE("TYPE");

		private String type;

		private Action(String type) {
			this.type = type;
		}

		public String getType() {
			return type;
		}
	}

	public PunishmentModificationRequest(UUID issuer, String target, int number, boolean canModifyOthers, Action action,
			String data) {
		this.issuer = issuer;
		this.target = target;
		this.number = number;
		this.canModifyOthers = canModifyOthers;
		this.action = action;
		this.data = data;
	}

	public UUID getIssuer() {
		return issuer;
	}

	public String getTarget() {
		return target;
	}

	public int getNumber() {
		return number;
	}

	public boolean getCanModifyOthers() {
		return canModifyOthers;
	}

	public Action getAction() {
		return action;
	}

	public String getData() {
		return data;
	}

	@Override
	public String getEndpoint() {
		return "/punishments/edit";
	}

	@Override
	public Class<Void> getResponseType() {
		return Void.class;
	}

	@Override
	public APIResponse<Void> makeRequest(APIClient client) {
		return client.post(this);
	}

}

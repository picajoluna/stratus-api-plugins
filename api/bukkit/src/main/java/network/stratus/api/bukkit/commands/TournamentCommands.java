/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.commands;

import app.ashcon.intake.Command;
import co.aikar.taskchain.TaskChain;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.bukkit.requests.teams.TeamRosterRequest;
import network.stratus.api.bukkit.responses.teams.TeamRosterResponse;
import network.stratus.api.bukkit.util.JWT;
import network.stratus.api.bukkit.util.Pair;
import network.stratus.api.bukkit.util.Util;
import network.stratus.api.client.APIResponse;
import network.stratus.api.client.ResponseStatus;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Commands for teams to use for tournament registration
 *
 * @author Matthew Arnold
 */
public class TournamentCommands {

    private final JWT jwt;

    public TournamentCommands(JWT jwt) {
        this.jwt = jwt;
    }

    @Command(
            aliases = "register",
            desc = "Register your team for the tournament"
    )
    public void registerTeam(CommandSender sender) {
        if(!(sender instanceof Player)) {
            System.out.println("Not a player!");
            return;
        }

        Player player = (Player) sender;
        UUID uuid = player.getUniqueId();
        SingleAudience audience = new SingleAudience(player);

        roster(player)
                //async generate the token, then sync send it
                .async(this::tokenPair)
                .syncLast(x -> {
                    APIResponse<TeamRosterResponse> rosterResponse = x.first();
                    if(rosterResponse.getStatus() == ResponseStatus.SUCCESS) {
                        //at this point the person is on a team, token has already been generated
                        TeamRosterResponse roster = rosterResponse.getResponseObject();
                        if(!roster.getLeader().equals(uuid)) {
                            //player is not the leader, reject the request
                            audience.sendMessage("tournament.register.needleader");
                            return;
                        }
                        //player is the leader, generate a JWT for them and send it their way -- good boi
                        TextComponent message = new TextComponent(Util.translate(player, "tournament.register.link"));
                        message.setClickEvent( new ClickEvent( ClickEvent.Action.OPEN_URL, "https://tmreg.stratus.network/reg/" + x.second()));
                        message.setHoverEvent( new HoverEvent( HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Util.translate(player, "tournament.register.linkhover")).create() ) );

                        message.setColor(ChatColor.AQUA);
                        player.sendMessage(message);
                    }
                    else {
                        //person not on a team
                        audience.sendMessage("tournament.register.needleaderandteam");
                    }
                }).execute((e, task) -> Util.handleCommandException(e, task, sender));
    }

    //gets the roster for a certain player's team
    private TaskChain<APIResponse<TeamRosterResponse>> roster(Player player) {
        TeamRosterRequest request = new TeamRosterRequest(player.getUniqueId(), "");
        return StratusAPI.get()
                .newSharedChain("teams")
                .asyncFirst(() -> request.makeRequest(StratusAPI.get().getApiClient()));
    }

    //gets the pair of the jwt for a team for a certain roster response, or null if there is no roster response (the previous request failed)
    private Pair<APIResponse<TeamRosterResponse>, String> tokenPair(APIResponse<TeamRosterResponse> response) {
        if(response.getStatus() != ResponseStatus.SUCCESS) {
            //not success, therefore null token
            return Pair.of(response, null);
        }
        TeamRosterResponse roster = response.getResponseObject();
        return Pair.of(response, jwt.generateToken(roster.getTeamName()));
    }
}

/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.commands;

import java.util.UUID;

import javax.ws.rs.NotFoundException;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import app.ashcon.intake.Command;
import app.ashcon.intake.bukkit.parametric.annotation.Sender;
import app.ashcon.intake.parametric.annotation.Default;
import app.ashcon.intake.parametric.annotation.Text;
import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.Chat;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.bukkit.models.TabCompletePlayer;
import network.stratus.api.bukkit.requests.teams.TeamCreateRequest;
import network.stratus.api.bukkit.requests.teams.TeamDisbandRequest;
import network.stratus.api.bukkit.requests.teams.TeamInviteCheckRequest;
import network.stratus.api.bukkit.requests.teams.TeamInviteListRequest;
import network.stratus.api.bukkit.requests.teams.TeamInviteReplyRequest;
import network.stratus.api.bukkit.requests.teams.TeamInviteRequest;
import network.stratus.api.bukkit.requests.teams.TeamKickRequest;
import network.stratus.api.bukkit.requests.teams.TeamLeaderPromoteRequest;
import network.stratus.api.bukkit.requests.teams.TeamLeaveRequest;
import network.stratus.api.bukkit.requests.teams.TeamListRequest;
import network.stratus.api.bukkit.requests.teams.TeamRosterRequest;
import network.stratus.api.bukkit.responses.teams.TeamInviteCheckResponse;
import network.stratus.api.bukkit.responses.teams.TeamInviteListResponse;
import network.stratus.api.bukkit.responses.teams.TeamInviteResponse;
import network.stratus.api.bukkit.responses.teams.TeamLeaderPromoteResponse;
import network.stratus.api.bukkit.responses.teams.TeamListResponse;
import network.stratus.api.bukkit.responses.teams.TeamRosterResponse;
import network.stratus.api.bukkit.server.CooldownManager;
import network.stratus.api.bukkit.util.Util;
import network.stratus.api.client.APIResponse;
import network.stratus.api.client.ResponseStatus;

/**
 * Commands for managing teams.
 * 
 * @author Meeples10
 * @author Ian Ballingall
 *
 */
public class TeamCommands {

	// Use cooldown managers to perform confirmation
	private CooldownManager disbandCooldown;
	private CooldownManager promoteCooldown;

	public TeamCommands(long confirmationTime) {
		this.disbandCooldown = new CooldownManager(confirmationTime);
		this.promoteCooldown = new CooldownManager(confirmationTime);
	}

	@Command(aliases = "create",
			desc = "Create a new team",
			usage = "<name>",
			perms = "stratusapi.command.team.create")
	public void create(@Sender CommandSender sender, @Text String teamName) {
		TeamCreateRequest request = new TeamCreateRequest(((Player) sender).getUniqueId(), teamName);
		StratusAPI.get().newSharedChain("teams").<APIResponse<Void>>asyncFirst(() -> {
			return request.makeRequest(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			if (response.getStatus() == ResponseStatus.SUCCESS) {
				new SingleAudience(sender).sendMessage("team.create.success", teamName);
			} else {
				new SingleAudience(sender).sendMessage("team.create.failure", response.getDescription());
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = "disband",
			desc = "Disband your team",
			perms = "stratusapi.command.team.disband")
	public void delete(@Sender CommandSender sender) {
		UUID uuid = ((Player) sender).getUniqueId();
		SingleAudience audience = new SingleAudience(sender);
		// We're using the cooldown the opposite way around, but it does the job
		if (disbandCooldown.executeIfAllowed(uuid)) {
			audience.sendMessage("team.disband.confirm");
			return;
		}

		TeamDisbandRequest request = new TeamDisbandRequest(((Player) sender).getUniqueId());
		StratusAPI.get().newSharedChain("teams").<APIResponse<Void>>asyncFirst(() -> {
			return request.makeRequest(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			if (response.getStatus() == ResponseStatus.SUCCESS) {
				audience.sendMessage("team.disband.success", response.getDescription());
			} else {
				audience.sendMessage("team.disband.failure", response.getDescription());
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = "invite",
			desc = "Invite a player to your team",
			usage = "<username>",
			perms = "stratusapi.command.team.invite")
	public void invite(@Sender CommandSender sender, TabCompletePlayer target) {
		TeamInviteRequest request = new TeamInviteRequest(((Player) sender).getUniqueId(), target.getUsername(), false);
		StratusAPI.get().newSharedChain("teams").<APIResponse<TeamInviteResponse>>asyncFirst(() -> {
			return request.makeRequest(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			if (response.getStatus() == ResponseStatus.SUCCESS) {
				TeamInviteResponse responseObject = response.getResponseObject();
				Player targetPlayer = StratusAPI.get().getServer().getPlayer(responseObject.getTarget());

				new SingleAudience(sender).sendMessage("team.invite.success",
						(targetPlayer == null) ? Chat.OFFLINE_COLOR + responseObject.getTargetOfflineName()
						: targetPlayer.getDisplayName(), responseObject.getTeam());
				Util.sendTeamInviteMessage(responseObject.getTarget(), responseObject.getTeam());
			} else {
				new SingleAudience(sender).sendMessage("team.invite.failure", response.getDescription());
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = "uninvite",
			desc = "Cancel an invite to your team",
			usage = "<username>",
			perms = "stratusapi.command.team.invite")
	public void uninvite(@Sender CommandSender sender, TabCompletePlayer target) {
		TeamInviteRequest request = new TeamInviteRequest(((Player) sender).getUniqueId(), target.getUsername(), true);
		StratusAPI.get().newSharedChain("teams").<APIResponse<TeamInviteResponse>>asyncFirst(() -> {
			return request.makeRequest(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			if (response.getStatus() == ResponseStatus.SUCCESS) {
				TeamInviteResponse responseObject = response.getResponseObject();
				Player targetPlayer = StratusAPI.get().getServer().getPlayer(responseObject.getTarget());

				new SingleAudience(sender).sendMessage("team.invite.cancel.success",
						(targetPlayer == null) ? Chat.OFFLINE_COLOR + responseObject.getTargetOfflineName()
						: targetPlayer.getDisplayName(), responseObject.getTeam());
			} else {
				new SingleAudience(sender).sendMessage("team.invite.cancel.failure", response.getDescription());
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = { "accept", "join" },
			desc = "Accept an invite to a team",
			usage = "<team name>",
			perms = "stratusapi.command.team.invite")
	public void accept(@Sender CommandSender sender, @Text String teamName) {
		TeamInviteReplyRequest request = new TeamInviteReplyRequest(((Player) sender).getUniqueId(), teamName, true);
		StratusAPI.get().newSharedChain("teams").<APIResponse<Void>>asyncFirst(() -> {
			return request.makeRequest(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			if (response.getStatus() == ResponseStatus.SUCCESS) {
				new SingleAudience(sender).sendMessage("team.accept.success", response.getDescription());
			} else {
				new SingleAudience(sender).sendMessage("team.accept.failure", response.getDescription());
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = { "deny", "reject" },
			desc = "Reject an invitation to join a team",
			usage = "<team name>",
			perms = "stratusapi.command.team.invite")
	public void reject(@Sender CommandSender sender, @Text String teamName) {
		TeamInviteReplyRequest request = new TeamInviteReplyRequest(((Player) sender).getUniqueId(), teamName, false);
		StratusAPI.get().newSharedChain("teams").<APIResponse<Void>>asyncFirst(() -> {
			return request.makeRequest(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			if (response.getStatus() == ResponseStatus.SUCCESS) {
				new SingleAudience(sender).sendMessage("team.reject.success", response.getDescription());
			} else {
				new SingleAudience(sender).sendMessage("team.reject.failure", response.getDescription());
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = { "promote", "setleader" },
			desc = "Change the team leader",
			usage = "<username>",
			perms = "stratusapi.command.team.promote")
	public void promote(@Sender CommandSender sender, TabCompletePlayer target) {
		UUID uuid = ((Player) sender).getUniqueId();
		SingleAudience audience = new SingleAudience(sender);
		if (promoteCooldown.executeIfAllowed(uuid)) {
			audience.sendMessage("team.promote.confirm", target.getUsername());
			return;
		}
		
		TeamLeaderPromoteRequest request = new TeamLeaderPromoteRequest(uuid, target.getUsername());
		StratusAPI.get().newSharedChain("teams").<APIResponse<TeamLeaderPromoteResponse>>asyncFirst(() -> {
			return request.makeRequest(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			if (response.getStatus() == ResponseStatus.SUCCESS) {
				TeamLeaderPromoteResponse responseObject = response.getResponseObject();
				Player newLeader = StratusAPI.get().getServer().getPlayer(responseObject.getTarget());
				audience.sendMessage("team.promote.success", (newLeader == null) ? Chat.OFFLINE_COLOR
						+ responseObject.getTargetOfflineName() : newLeader.getDisplayName());

				if (newLeader != null) {
					new SingleAudience(newLeader).sendMessage("team.promote.recipient", responseObject.getTeam());
				}
			} else {
				audience.sendMessage("team.promote.failure", response.getDescription());
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = "roster",
			desc = "View the roster your team or the team with the given name",
			usage = "[<team name>]",
			perms = "stratusapi.command.team.roster")
	public void roster(@Sender CommandSender sender, @Default("") @Text String teamName) {
		TeamRosterRequest request = new TeamRosterRequest(
				(sender instanceof Player) ? ((Player) sender).getUniqueId() : null, teamName);
		StratusAPI.get().newSharedChain("teams").<APIResponse<TeamRosterResponse>>asyncFirst(() -> {
			return request.makeRequest(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			if (response.getStatus() == ResponseStatus.SUCCESS) {
				TeamRosterResponse responseObject = response.getResponseObject();
				Player leader = StratusAPI.get().getServer().getPlayer(responseObject.getLeader());
				SingleAudience audience = new SingleAudience(sender);

				audience.sendMessage("team.roster.title", responseObject.getTeamName());
				audience.sendMessage("team.roster.leader", (leader == null) ? Chat.OFFLINE_COLOR
						+ responseObject.getLeaderOfflineName() : leader.getDisplayName());

				response.getResponseObject().getTeamMembers().forEach((uuid, username) -> {
					Player player = StratusAPI.get().getServer().getPlayer(uuid);
					audience.sendMessageRaw((player == null) ? Chat.OFFLINE_COLOR + username
							: player.getDisplayName());
				});
			} else {
				new SingleAudience(sender).sendMessage("team.roster.failure", response.getDescription());
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = "list",
			desc = "List all the teams that exist",
			usage = "[<page>]",
			perms = "stratusapi.command.team.list")
	public void list(CommandSender sender, @Default("1") int page) {
		TeamListRequest request = new TeamListRequest(page);
		StratusAPI.get().newSharedChain("teams").<APIResponse<TeamListResponse>>asyncFirst(() -> {
			return request.makeRequest(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			if (response.getStatus() == ResponseStatus.SUCCESS) {
				SingleAudience audience = new SingleAudience(sender);
				TeamListResponse list = response.getResponseObject();

				if (list.getTeams().isEmpty()) {
					audience.sendMessage("team.list.empty");
				} else {
					audience.sendMessage("team.list.title", page);
					list.getTeams().forEach(team -> {
						Player leaderPlayer = StratusAPI.get().getServer().getPlayer(team.getLeader().get_id());
						audience.sendMessage("team.list.entry", team.getName(), (leaderPlayer == null) ? Chat.OFFLINE_COLOR
								+ team.getLeader().getUsername() : leaderPlayer.getDisplayName());
					});
				}
			} else {
				new SingleAudience(sender).sendMessage("team.list.failure", response.getDescription());
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = "leave",
			desc = "Leave your team",
			perms = "stratusapi.command.team.leave")
	public void leave(@Sender CommandSender sender) {
		TeamLeaveRequest request = new TeamLeaveRequest(((Player) sender).getUniqueId());
		StratusAPI.get().newSharedChain("teams").<APIResponse<Void>>asyncFirst(() -> {
			return request.makeRequest(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			if (response.getStatus() == ResponseStatus.SUCCESS) {
				new SingleAudience(sender).sendMessage("team.leave.success");
			} else {
				new SingleAudience(sender).sendMessage("team.leave.failure", response.getDescription());
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = "kick",
			desc = "Remove a player from your team",
			usage = "<username>",
			perms = "stratusapi.command.team.kick")
	public void kick(@Sender CommandSender sender, TabCompletePlayer target) {
		TeamKickRequest request = new TeamKickRequest(((Player) sender).getUniqueId(), target.getUsername());
		StratusAPI.get().newSharedChain("teams").<APIResponse<Void>>asyncFirst(() -> {
			return request.makeRequest(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			if (response.getStatus() == ResponseStatus.SUCCESS) {
				new SingleAudience(sender).sendMessage("team.kick.success", target.getUsername());
			} else {
				new SingleAudience(sender).sendMessage("team.kick.failure", response.getDescription());
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = "invites",
			desc = "View players with pending invites to join your team, or see invitations you've received "
					+ "if you aren't on a team",
			perms = "stratusapi.command.team.invite.list")
	public void listInvites(@Sender CommandSender sender) {
		TeamInviteListRequest request = new TeamInviteListRequest(((Player) sender).getUniqueId());
		StratusAPI.get().newSharedChain("teams").<APIResponse<TeamInviteListResponse>>asyncFirst(() -> {
			try {
				// Attempt to get invites for the player's team
				return request.makeRequest(StratusAPI.get().getApiClient());
			} catch (NotFoundException e) {
				// Show the player's received invites if they aren't on a team
				listReceivedInvites(sender);
				return null;
			}
		}).abortIfNull().syncLast(response -> {
			TeamInviteListResponse responseObject = response.getResponseObject();
			SingleAudience audience = new SingleAudience(sender);
			if (responseObject.getInvitees().isEmpty()) {
				audience.sendMessage("team.invite.list.empty", responseObject.getTeamName());
			} else {
				audience.sendMessage("team.invite.list.title", responseObject.getTeamName());
				responseObject.getInvitees().forEach((uuid, username) -> {
					Player player = StratusAPI.get().getServer().getPlayer(uuid);
					audience.sendMessageRaw((player == null) ? Chat.OFFLINE_COLOR + username
							: player.getDisplayName());
				});
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	/**
	 * See the invites to teams this player has received.
	 * 
	 * @param sender The sender's {@link UUID}
	 */
	private void listReceivedInvites(CommandSender sender) {
		TeamInviteCheckRequest request = new TeamInviteCheckRequest(((Player) sender).getUniqueId());
		StratusAPI.get().newSharedChain("teams").<APIResponse<TeamInviteCheckResponse>>asyncFirst(() -> {
			return request.makeRequest(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			if (response.getStatus() == ResponseStatus.SUCCESS) {
				if (response.getResponseObject().getInvites().isEmpty()) {
					new SingleAudience(sender).sendMessage("team.invite.recipient.none");
				} else {
					new SingleAudience(sender).sendMessage("team.invite.recipient.title");
					response.getResponseObject().listInvites();
				}
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

}

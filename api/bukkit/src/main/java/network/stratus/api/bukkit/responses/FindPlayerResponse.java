/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.responses;

import java.util.Date;
import java.util.UUID;

/**
 * Represents a response to a
 * {@link network.stratus.api.bukkit.requests.FindPlayerRequest}.
 * 
 * @author Ian Ballingall
 *
 */
public class FindPlayerResponse {

	private UUID uuid;
	private String username;
	private boolean onlineStatus;
	private String server;
	private Date lastSeen;

	public UUID getUuid() {
		return uuid;
	}

	public String getUsername() {
		return username;
	}

	public boolean getOnlineStatus() {
		return onlineStatus;
	}

	public String getServer() {
		return server;
	}

	public Date getLastSeen() {
		return lastSeen;
	}

}

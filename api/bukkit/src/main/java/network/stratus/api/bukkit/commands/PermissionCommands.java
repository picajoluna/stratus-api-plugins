/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.commands;

import java.util.UUID;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import app.ashcon.intake.Command;
import app.ashcon.intake.CommandException;
import app.ashcon.intake.parametric.annotation.Switch;
import app.ashcon.intake.parametric.annotation.Text;
import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.bukkit.models.TabCompletePlayer;
import network.stratus.api.bukkit.permissions.BukkitPermissionsManager;
import network.stratus.api.bukkit.requests.GroupMembershipModificationRequest;
import network.stratus.api.bukkit.responses.GroupMembershipModificationResponse;
import network.stratus.api.bukkit.util.Util;
import network.stratus.api.client.APIResponse;
import network.stratus.api.client.ResponseStatus;
import network.stratus.api.permissions.Group;
import network.stratus.api.requests.PermissionsRefreshRequest;

/**
 * Represents the commands associated with the permissions system.
 * 
 * @author Ian Ballingall
 *
 */
public class PermissionCommands {

	@Command(aliases = "refresh",
			desc = "Reloads the permissions files",
			perms = "stratusapi.command.permissions.refresh")
	public void refresh(CommandSender sender, @Switch('g') boolean globalReload) {
		if (globalReload) {
			StratusAPI.get().newSharedChain("groups").<APIResponse<Void>>asyncFirst(() -> {
				return new PermissionsRefreshRequest().makeRequest(StratusAPI.get().getApiClient());
			}).syncLast(response -> {
				SingleAudience audience = new SingleAudience(sender);
		
				if (response.getStatus() == ResponseStatus.SUCCESS) {
					audience.sendMessage("permissions.refresh.success");
					audience.sendMessage("permissions.refresh.updatewarning");
				} else {
					audience.sendMessage("permissions.refresh.failure", response.getDescription());
				}
	
				StratusAPI.get().getPermissionsManager().getGroupsManager().clearGroups();
			}).execute((e, task) -> Util.handleCommandException(e, task, sender));
		} else {
			SingleAudience audience = new SingleAudience(sender);
			StratusAPI.get().getPermissionsManager().getGroupsManager().clearGroups();
			audience.sendMessage("permissions.refresh.success");
			audience.sendMessage("permissions.refresh.updatewarning");
		}
	}

	@Command(aliases = "group",
			desc = "Changes a user's membership of the specified group",
			perms = "stratusapi.command.permissions.group",
			usage = "add|remove <user> <group>")
	public void addToGroup(CommandSender sender, String action, TabCompletePlayer player, @Text String groupName) throws CommandException {
		// Obtain the action
		boolean add = action.equals("add");
		if (!add && !action.equals("remove")) {
			throw new CommandException("Invalid action specified: " + action);
		}
		
		// Obtain the issuer UUID if applicable
		UUID issuer = (sender instanceof Player) ? ((Player) sender).getUniqueId() : null;
		BukkitPermissionsManager perms = StratusAPI.get().getPermissionsManager();

		// Make API request on the groups chain
		StratusAPI.get().newSharedChain("groups").<APIResponse<GroupMembershipModificationResponse>>asyncFirst(() -> {
			APIResponse<GroupMembershipModificationResponse> response = new GroupMembershipModificationRequest(issuer, player.getUsername(),
					groupName, add).makeRequest(StratusAPI.get().getApiClient());
			return response;
		}).<GroupMembershipModificationResponse>sync(response -> {
			SingleAudience audience = new SingleAudience(sender);

			if (response.getStatus() == ResponseStatus.SUCCESS) {
				audience.sendMessage("permissions.group.update.success");
				return response.getResponseObject();
			} else {
				audience.sendMessage("permissions.group.update.failure", response.getDescription());
				return null;
			}
		}).abortIfNull().<Group>async(responseObject -> {
			return perms.getGroupsManager().getStoredGroupById(responseObject.getGroupId());
		}).syncLast(group -> {
			// Update the player's permissions if they're online
			if (player.getPlayer().isPresent()) {
				Player playerObject = player.getPlayer().get();
				if (add) {
					perms.setPermissions(playerObject, group);
					StratusAPI.get().getDisplayNameManager().setDisplayName(playerObject);
					new SingleAudience(playerObject).sendMessage("permissions.updated");
				}
				// TODO: handle safely unsetting permissions
				// We don't want to unset the permissions if they are also part of another group
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

}

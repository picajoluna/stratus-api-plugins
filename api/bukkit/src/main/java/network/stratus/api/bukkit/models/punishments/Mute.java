/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.models.punishments;

import java.util.Date;
import java.util.UUID;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.MultiAudience;
import network.stratus.api.bukkit.chat.MuteManager;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.bukkit.tasks.TimedPunishmentTask;
import network.stratus.api.bukkit.util.Util;
import network.stratus.api.models.punishment.PunishmentFactory.Type;

/**
 * Represents a mute, preventing a {@link Player} from interacting in chat.
 * 
 * @author Ian Ballingall
 *
 */
public class Mute extends TimedBukkitPunishment {

	public Mute(String _id, UUID issuer, String issuerOfflineName, UUID target, String targetOfflineName, Date time,
			Date expiry, String reason, boolean active, int number, String serverName, long timeRemaining) {
		super(_id, issuer, issuerOfflineName, target, targetOfflineName, time, expiry, reason, active, number,
				serverName, timeRemaining);
	}

	@Override
	public void enforce(boolean showServer) {
		initialisePlayers();
		formatNames();
		
		MultiAudience staff = new MultiAudience.Builder().includePermission("stratusapi.punishments.mutes.see.others").build();

		Date end = new Date(new Date().getTime() + getTimeRemaining());
		if (showServer) {
			staff.sendMessage("punishment.mute.broadcast.server", serverName,
					(issuerPlayer == null) ? issuerOfflineName : issuerPlayer.getDisplayName(),
					(targetPlayer == null) ? targetOfflineName : targetPlayer.getDisplayName(),
					Util.differenceInHours(time, end), reason);
		} else {
			staff.sendMessage("punishment.mute.broadcast",
					(issuerPlayer == null) ? issuerOfflineName : issuerPlayer.getDisplayName(),
					(targetPlayer == null) ? targetOfflineName : targetPlayer.getDisplayName(),
					Util.differenceInHours(time, end), reason);
		}

		if (targetPlayer != null) {
			MuteManager manager = StratusAPI.get().getMuteManager();
			manager.addMute(this);
			SingleAudience audience = new SingleAudience(targetPlayer);
			audience.sendMessage("punishment.mute.message.1");
			audience.sendMessage("punishment.mute.message.2", reason);
			audience.sendMessage("punishment.mute.message.3",
					StratusAPI.get().getTranslator().getTimeDifferenceString(targetPlayer.getLocale(), end, time));

			if (manager.isOnlineTimeGameplay()) {
				manager.addMuteTask(getTarget(),
						new TimedPunishmentTask.Builder(this).setPeriodToConfigurationValue().build().execute());
			}
		}
	}

	@Override
	public Type getType() {
		return Type.MUTE;
	}

}

/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.tasks;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.WebApplicationException;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.bukkit.requests.TimedPunishmentDurationUpdateRequest;
import network.stratus.api.models.punishment.TimedPunishment;

/**
 * A task which automatically updates the 'gameplay' time remaining on a timed
 * {@link Punishment}. An instance of this task will be launched automatically
 * if onlineTimeIsGameplay is true; otherwise, this should be manually launched
 * in the appropriate place.
 * 
 * @author Ian Ballingall
 *
 */
public class TimedPunishmentTask extends BukkitRunnable {

	/** The {@link Punishment} being monitored and updated. */
	private final TimedPunishment punishment;
	/** The interval between updates in milliseconds. */
	private final long period;

	/**
	 * Directly create a new which monitors and updates a timed {@link Punishment}.
	 * This task involves I/O and should be run asynchronously!
	 * 
	 * @param punishment The {@link Punishment} to monitor and update
	 * @param period     The interval between updates
	 */
	public TimedPunishmentTask(TimedPunishment punishment, long period) {
		this.punishment = punishment;
		this.period = period;
	}

	@Override
	public void run() {
		punishment.setTimeRemaining(punishment.getTimeRemaining() - period); // Decrement the locally stored duration
		TimedPunishmentDurationUpdateRequest request = new TimedPunishmentDurationUpdateRequest(punishment.get_id(),
				period); // API expects milliseconds
		// Attempt to update this on the API, but don't crash the thread if it fails for
		// some reason
		try {
			request.makeRequest(StratusAPI.get().getApiClient());
		} catch (ProcessingException | WebApplicationException e) {
			e.printStackTrace();
		}

		if (punishment.getTimeRemaining() <= 0) {
			// Notify the player their mute has expired
			new BukkitRunnable() {

				@Override
				public void run() {
					Player player = StratusAPI.get().getServer().getPlayer(punishment.getTarget());
					if (player != null) {
						new SingleAudience(player).sendMessage("punishment.mute.expired");
					}
				}

			}.runTask(StratusAPI.get());

			this.cancel();
		}
	}

	/**
	 * A builder to help in constructing this task consistently.
	 */
	public static class Builder {

		/** The punishment to be monitored and updated. */
		private TimedPunishment punishment;
		/** The interval between updates in milliseconds. */
		private long period;
		/** The interval between updates in server ticks. */
		private long ticks;
		/** The resulting task instance. */
		private TimedPunishmentTask result;

		/**
		 * Begin constructing a new task. Initialises period to a default of one minute.
		 * 
		 * @param punishment The punishment this task will monitor and update
		 */
		public Builder(TimedPunishment punishment) {
			this.punishment = punishment;
			this.period = 60000;
			this.ticks = 1200; // 1200 ticks = 1 minute
		}

		/**
		 * Sets the update period in server ticks. The millisecond time is also updated.
		 * 
		 * @param ticks The number of ticks between updates
		 * @return The builder
		 */
		public Builder setPeriodInTicks(long ticks) {
			this.ticks = ticks;
			this.period = ticks * 50;
			return this;
		}

		/**
		 * Sets the update period in milliseconds. The tick period is also updated.
		 * 
		 * @param period The number of milliseconds between updates
		 * @return The builder
		 */
		public Builder setPeriodInMilliseconds(long period) {
			this.period = period;
			this.ticks = (long) (period * 0.02); // 20 ticks per second
			return this;
		}

		/**
		 * Sets the update period to the number specified in the configuration file as
		 * mutes.updatePeriod.
		 * 
		 * @return The builder
		 */
		public Builder setPeriodToConfigurationValue() {
			return setPeriodInMilliseconds(StratusAPI.get().getMuteManager().getUpdatePeriod());
		}

		/**
		 * Constructs the task object.
		 * 
		 * @return The builder
		 */
		public Builder build() {
			result = new TimedPunishmentTask(punishment, period);
			return this;
		}

		/**
		 * Returns the task object without executing it.
		 * 
		 * @return The task object
		 */
		public TimedPunishmentTask getResult() {
			return result;
		}

		/**
		 * Executes the task to run asynchronously on the provided update period.
		 * 
		 * @return The task object
		 */
		public BukkitTask execute() {
			return result.runTaskTimerAsynchronously(StratusAPI.get(), ticks, ticks);
		}

	}

}

/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.listeners;

import java.util.Date;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.MuteManager;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.bukkit.models.LoginData;
import network.stratus.api.bukkit.requests.JoinRequest;
import network.stratus.api.bukkit.requests.teams.TeamInviteCheckRequest;
import network.stratus.api.bukkit.responses.teams.TeamInviteCheckResponse;
import network.stratus.api.bukkit.tasks.TimedPunishmentTask;
import network.stratus.api.client.APIResponse;
import network.stratus.api.client.ResponseStatus;
import network.stratus.api.models.punishment.TimedPunishment;

/**
 * Contains any listeners pertaining to users joining the server. These occur
 * after a successful login, when the player is entering the world.
 * 
 * @author Ian Ballingall
 *
 */
public class JoinListener implements Listener {

	/**
	 * Called when the user successfully joins the server. Mutes are applied and the
	 * API is notified of the player's successful join.
	 * 
	 * @param event The join event
	 */
	@EventHandler(priority = EventPriority.MONITOR)
	public void onJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		JoinRequest requestObject = new JoinRequest(player.getUniqueId(), player.getName(),
				player.getAddress().getHostString());

		StratusAPI.get().getDisplayNameManager().setDisplayName(player);

		// Apply the player's mute if it exists
		LoginData loginData = StratusAPI.get().getLoginManager().popLoginData(player.getUniqueId());
		TimedPunishment mute = loginData.getMute();
		if (mute != null) {
			MuteManager muteManager = StratusAPI.get().getMuteManager();
			muteManager.addMute(mute);
			Date now = new Date();
			Date end = new Date(now.getTime() + mute.getTimeRemaining());
			SingleAudience audience = new SingleAudience(event.getPlayer());
			audience.sendMessage("punishment.mute.message.1");
			audience.sendMessage("punishment.mute.message.2", mute.getReason());
			audience.sendMessage("punishment.mute.message.3",
					StratusAPI.get().getTranslator().getTimeDifferenceString(event.getPlayer().getLocale(), end, now));

			// Set up timed punishment task to track gameplay time
			if (muteManager.isOnlineTimeGameplay()) {
				BukkitTask task = new TimedPunishmentTask.Builder(mute).setPeriodToConfigurationValue().build()
						.execute();
				muteManager.addMuteTask(player.getUniqueId(), task);
			}
		}

		// Send the join information to the API
		new BukkitRunnable() {
			@Override
			public void run() {
				requestObject.makeRequest(StratusAPI.get().getApiClient());
			}
		}.runTaskAsynchronously(StratusAPI.get());
	}

	/**
	 * Checks for a player having invites to teams when joining.
	 * 
	 * @param event The join event
	 */
	@EventHandler(priority = EventPriority.MONITOR)
	public void checkTeamInvites(PlayerJoinEvent event) {
		TeamInviteCheckRequest request = new TeamInviteCheckRequest(event.getPlayer().getUniqueId());
		StratusAPI.get().newSharedChain("teams").<APIResponse<TeamInviteCheckResponse>>asyncFirst(() -> {
			return request.makeRequest(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			if (response.getStatus() == ResponseStatus.SUCCESS) {
				response.getResponseObject().listInvites();
			}
		}).execute();
	}

}

/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.models.punishments;

import java.util.Date;
import java.util.UUID;

import javax.annotation.Nullable;

import org.bukkit.entity.Player;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.Chat;
import network.stratus.api.models.punishment.Punishment;
import network.stratus.api.models.punishment.TimedPunishment;

/**
 * Abstract representation of a {@link TimedPunishment}, optionally
 * encapsulating the {@link Player}s who issued and receive the punishment.
 * 
 * @author Ian Ballingall
 *
 */
public abstract class TimedBukkitPunishment extends TimedPunishment {

	protected @Nullable Player issuerPlayer;
	protected @Nullable Player targetPlayer;

	public TimedBukkitPunishment(String _id, UUID issuer, String issuerOfflineName, UUID target,
			String targetOfflineName, Date time, Date expiry, String reason, boolean active, int number,
			String serverName, long timeRemaining) {
		super(_id, issuer, issuerOfflineName, target, targetOfflineName, time, expiry, reason, active, number,
				serverName, timeRemaining);
	}

	@Override
	public Punishment initialisePlayers() {
		this.issuerPlayer = (issuer == null) ? null : StratusAPI.get().getServer().getPlayer(issuer);
		this.targetPlayer = StratusAPI.get().getServer().getPlayer(target);
		return this;
	}

	@Override
	public Punishment formatNames() {
		this.issuerOfflineName = Chat.OFFLINE_COLOR + this.issuerOfflineName;
		this.targetOfflineName = Chat.OFFLINE_COLOR + this.targetOfflineName;
		return this;
	}

}

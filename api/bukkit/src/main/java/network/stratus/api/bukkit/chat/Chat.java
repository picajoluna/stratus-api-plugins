/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.chat;

import org.bukkit.ChatColor;

/**
 * Chat-related constants.
 * 
 * @author Ian Ballingall
 *
 */
public class Chat {

	// Perhaps these should be configurable?

	/** The colour for offline players' names. */
	public static ChatColor OFFLINE_COLOR = ChatColor.DARK_AQUA;
	/** The default colour for online players' names. */
	public static ChatColor ONLINE_COLOR_DEFAULT = ChatColor.AQUA;

}

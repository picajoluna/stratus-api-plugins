/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.requests.teams;

import network.stratus.api.client.APIClient;
import network.stratus.api.client.APIRequest;
import network.stratus.api.client.APIResponse;

/**
 * A request relating to administration of teams. Implementing classes should
 * set the endpoint this request is sent to.
 * 
 * @author Ian Ballingall
 *
 */
public abstract class TeamAdminRequest implements APIRequest<Void> {

	private String teamName;
	private String target;

	public TeamAdminRequest(String teamName, String target) {
		this.teamName = teamName;
		this.target = target;
	}

	public String getTeamName() {
		return teamName;
	}

	public String getTarget() {
		return target;
	}

	@Override
	public Class<Void> getResponseType() {
		return Void.class;
	}

	@Override
	public APIResponse<Void> makeRequest(APIClient client) {
		return client.post(this);
	}

}

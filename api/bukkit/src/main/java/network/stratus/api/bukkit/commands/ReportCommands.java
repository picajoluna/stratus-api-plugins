/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.commands;

import java.util.Date;
import java.util.UUID;

import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import app.ashcon.intake.Command;
import app.ashcon.intake.CommandException;
import app.ashcon.intake.parametric.annotation.Default;
import app.ashcon.intake.parametric.annotation.Range;
import app.ashcon.intake.parametric.annotation.Switch;
import app.ashcon.intake.parametric.annotation.Text;
import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.Chat;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.bukkit.models.Report;
import network.stratus.api.bukkit.models.TabCompletePlayer;
import network.stratus.api.bukkit.requests.ReportListRequest;
import network.stratus.api.bukkit.responses.ReportListResponse;
import network.stratus.api.bukkit.server.CooldownManager;
import network.stratus.api.bukkit.util.Util;
import network.stratus.api.client.APIResponse;
import network.stratus.api.client.ResponseStatus;

/**
 * Commands for reporting players and viewing reports.
 * 
 * @author Ian Ballingall
 *
 */
public class ReportCommands {

	private CooldownManager reportCooldown;

	public ReportCommands(long cooldownTime) {
		this.reportCooldown = new CooldownManager(cooldownTime);
	}

	@Command(aliases = "report",
			desc = "Report a player who is breaking the rules",
			perms = "stratusapi.command.report",
			usage = "<player> <reason>")
	public void onReport(CommandSender sender, Player target, @Text String reason) {
		Player reporterPlayer = (sender instanceof Player) ? (Player) sender : null;
		UUID reporterUuid = (reporterPlayer == null) ? null : reporterPlayer.getUniqueId();
		if (reporterUuid != null) {
			if (!reportCooldown.executeIfAllowed(reporterUuid)) {
				new SingleAudience(sender).sendMessage("report.cooldown",
						(reportCooldown.getLastExecutionTime(reporterUuid)
						+ reportCooldown.getCooldownTime()
						- System.currentTimeMillis()) / 1000);
				return;
			}
		}
		
		Report report = new Report(reporterUuid, sender.getName(), target.getUniqueId(), target.getName(), new Date(),
				reason, StratusAPI.get().getServerName());

		StratusAPI.get().newSharedChain("reports").<APIResponse<Void>>asyncFirst(() -> {
			return report.makeRequest(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			SingleAudience senderAudience = new SingleAudience(sender);
			if (response.getStatus() == ResponseStatus.SUCCESS) {
				senderAudience.sendMessage("report.submitted");
				report.broadcast(reporterPlayer, target, false);
			} else {
				senderAudience.sendMessage("report.failure");
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}
	
	@Command(aliases = "reports",
			desc = "View the list of reports",
			perms = "stratusapi.command.reports",
			usage = "[-a] [-p <player>] [-s <server>] [<page>]",
			flags = "asp")
	public void listReports(CommandSender sender, @Default("1") @Range(min = 1) int page, @Switch('p') TabCompletePlayer player,
			@Switch('s') String server, @Switch('a') boolean allServers) {
		ReportListRequest request = new ReportListRequest(allServers, server, player.getUsername(), page);

		StratusAPI.get().newSharedChain("reports").<APIResponse<ReportListResponse>>asyncFirst(() -> {
			return request.makeRequest(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			SingleAudience audience = new SingleAudience(sender);
			if (response.getStatus() == ResponseStatus.SUCCESS) {
				ReportListResponse responseObject = response.getResponseObject();
				if (responseObject.getAllServers()) {
					audience.sendMessage("report.list.title.allservers");
				} else if (responseObject.getTarget() == null) {
					audience.sendMessage("report.list.title", responseObject.getServer());
				} else {
					Player target = StratusAPI.get().getServer().getPlayer(responseObject.getTarget());
					audience.sendMessage("report.list.title",
							(target == null) ? Chat.OFFLINE_COLOR + responseObject.getTargetOfflineName() : target.getDisplayName());
				}

				for (Report report : responseObject.getList()) {
					Player reporter = StratusAPI.get().getServer().getPlayer(report.getReporter());
					Player target = StratusAPI.get().getServer().getPlayer(report.getTarget());

					if (allServers) {
						audience.sendMessage("report.list.entry.includeserver", report.getServerName(),
								StratusAPI.get().getTranslator().getTimeDifferenceString(sender.getLocale(), report.getTime(),
								new Date()), (reporter == null) ? Chat.OFFLINE_COLOR + report.getReporterOfflineName() : reporter.getDisplayName(),
								(target == null) ? Chat.OFFLINE_COLOR + report.getTargetOfflineName() : target.getDisplayName(),
								report.getReason());
					} else {
						audience.sendMessage("report.list.entry",
								StratusAPI.get().getTranslator().getTimeDifferenceString(sender.getLocale(), report.getTime(), new Date()),
								(reporter == null) ? Chat.OFFLINE_COLOR + report.getReporterOfflineName() : reporter.getDisplayName(),
								(target == null) ? Chat.OFFLINE_COLOR + report.getTargetOfflineName() : target.getDisplayName(),
								report.getReason());
					}
				}
			} else {
				audience.sendMessage("report.list.failure", response.getDescription());
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	/**
	 * An alias for /report which can be run from console only. This is a workaround for
	 * an Intake bug which prevents console from correctly sending commands with usernames.
	 */
	@Command(aliases = "creport",
			desc = "Console report command",
			perms = "stratusapi.command.report.console",
			usage = "<player> <reason")
	public void consoleReport(CommandSender sender, String target, @Text String reason) throws CommandException {
		if (!(sender instanceof ConsoleCommandSender)) {
			throw new CommandException("This command is for console use only");
		}

		Player player = StratusAPI.get().getServer().getPlayer(target);
		if (player == null) {
			throw new CommandException("Player not found");
		}

		onReport(sender, player, reason);
	}
	
}

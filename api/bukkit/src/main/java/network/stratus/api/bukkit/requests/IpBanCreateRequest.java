/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.requests;

import javax.annotation.Nullable;

import network.stratus.api.client.APIClient;
import network.stratus.api.client.APIRequest;
import network.stratus.api.client.APIResponse;

/**
 * A request to create an IP ban.
 * 
 * @author Ian Ballingall
 *
 */
public class IpBanCreateRequest implements APIRequest<Void> {

	private String address;
	@Nullable
	private String endAddress;
	private String description;

	public IpBanCreateRequest(String address, @Nullable String endAddress, String description) {
		this.address = address;
		this.endAddress = endAddress;
		this.description = description;
	}

	public String getAddress() {
		return address;
	}

	@Nullable
	public String getEndAddress() {
		return endAddress;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public String getEndpoint() {
		return "/punishments/ip/create";
	}

	@Override
	public Class<Void> getResponseType() {
		return Void.class;
	}

	@Override
	public APIResponse<Void> makeRequest(APIClient client) {
		return client.post(this);
	}

}

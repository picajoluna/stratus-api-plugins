/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.requests.QuitRequest;

/**
 * Contains listeners pertaining to user disconnections.
 * 
 * @author Ian Ballingall
 *
 */
public class QuitListener implements Listener {

	/**
	 * Called when the user disconnects from the server. This detaches the user from
	 * the plugin with respect to permissions handling and notifies the API so that
	 * sessions can be appropriately terminated.
	 * 
	 * @param event The quit event
	 */
	@EventHandler(priority = EventPriority.MONITOR)
	public void onQuit(PlayerQuitEvent event) {
		StratusAPI.get().getPermissionsManager().detachPermissions(event.getPlayer());
		StratusAPI.get().getMuteManager().removeMute(event.getPlayer().getUniqueId());
		StratusAPI.get().getMuteManager().removeMuteTask(event.getPlayer().getUniqueId());
		QuitRequest request = new QuitRequest(event.getPlayer().getUniqueId());

		StratusAPI.get().newChain().async(() -> {
			request.makeRequest(StratusAPI.get().getApiClient());
		}).execute();
	}

}

/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.requests.teams;

import java.util.UUID;

import network.stratus.api.client.APIClient;
import network.stratus.api.client.APIRequest;
import network.stratus.api.client.APIResponse;

/**
 * A request to kick a player from this team.
 * 
 * @author Ian Ballingall
 *
 */
public class TeamKickRequest implements APIRequest<Void> {

	private UUID sender;
	private String target;

	public TeamKickRequest(UUID sender, String target) {
		this.sender = sender;
		this.target = target;
	}

	public UUID getSender() {
		return sender;
	}

	public String getTarget() {
		return target;
	}

	@Override
	public String getEndpoint() {
		return "/teams/kick";
	}

	@Override
	public Class<Void> getResponseType() {
		return Void.class;
	}

	@Override
	public APIResponse<Void> makeRequest(APIClient client) {
		return client.post(this);
	}

}

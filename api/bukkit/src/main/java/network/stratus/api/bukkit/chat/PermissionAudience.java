/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.chat;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import network.stratus.api.bukkit.StratusAPI;

/**
 * An audience of all {@link Player}s who have a given permission node.
 * 
 * @author Ian Ballingall
 * 
 * @deprecated Very inflexible class, restricted to one node. Use
 *             {@link MultiAudience.Builder#includePermission(String)} (and
 *             other associated methods, as required) instead. This will be
 *             removed as part of plugin version 7.0.0.
 *
 */
@Deprecated
public class PermissionAudience extends MultiAudience {

	private String permissionNode;

	public PermissionAudience(String permissionNode) {
		super(createTargets(permissionNode));
		this.permissionNode = permissionNode;
	}

	public String getPermissionNode() {
		return permissionNode;
	}

	private static List<CommandSender> createTargets(String node) {
		List<CommandSender> targets = new ArrayList<>();
		for (Player player : StratusAPI.get().getServer().getOnlinePlayers()) {
			if (player.hasPermission(node)) {
				targets.add(player);
			}
		}

		return targets;
	}

}

/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.models;

import java.util.Date;
import java.util.UUID;

import javax.annotation.Nullable;

import org.bukkit.Sound;
import org.bukkit.entity.Player;

import com.fasterxml.jackson.annotation.JsonIgnore;

import network.stratus.api.bukkit.chat.Chat;
import network.stratus.api.bukkit.chat.MultiAudience;
import network.stratus.api.client.APIClient;
import network.stratus.api.client.APIRequest;
import network.stratus.api.client.APIResponse;

/**
 * Represents a report made by one player against another. As reports are made
 * by one online player against another, all the data already exists on the
 * server when the player is reported, so this can double-up as a request which
 * doesn't require any data to be returned.
 * 
 * @author Ian Ballingall
 *
 */
public class Report implements APIRequest<Void> {

	/** The UUID of the reporter. */
	private UUID reporter;
	/** The offline name of the reporter. */
	private String reporterOfflineName;
	/** The UUID of the report target. */
	private UUID target;
	/** The offline name of the target. */
	private String targetOfflineName;
	/** The time of issue. */
	private Date time;
	/** The reason for the punishment. */
	private String reason;
	/** The name of the server this report was issued on. */
	private String serverName;

	/**
	 * Empty constructor to allow creation by the API.
	 */
	public Report() {
	}

	public Report(UUID reporter, String reporterOfflineName, UUID target, String targetOfflineName, Date time,
			String reason, String serverName) {
		this.reporter = reporter;
		this.reporterOfflineName = reporterOfflineName;
		this.target = target;
		this.targetOfflineName = targetOfflineName;
		this.time = time;
		this.reason = reason;
		this.serverName = serverName;
	}

	public UUID getReporter() {
		return reporter;
	}

	public String getReporterOfflineName() {
		return reporterOfflineName;
	}

	public UUID getTarget() {
		return target;
	}

	public String getTargetOfflineName() {
		return targetOfflineName;
	}

	public Date getTime() {
		return time;
	}

	public String getReason() {
		return reason;
	}

	public String getServerName() {
		return serverName;
	}

	@Override
	public String getEndpoint() {
		return "/reports/create";
	}

	@Override
	public Class<Void> getResponseType() {
		return Void.class;
	}

	@Override
	public APIResponse<Void> makeRequest(APIClient client) {
		return client.put(this);
	}

	/**
	 * Broadcasts the report message to staff members.
	 * 
	 * @param reporter   The {@link Player} object for the reporter
	 * @param target     The {@link Player} object for the target
	 * @param showServer Whether to include the server in the message
	 */
	@JsonIgnore
	public void broadcast(@Nullable Player reporter, @Nullable Player target, boolean showServer) {
		MultiAudience staff = new MultiAudience.Builder().includePermission("stratusapi.reports.see").build();
		if (showServer) {
			staff.sendMessage("report.broadcast.server", serverName,
					(reporter == null) ? Chat.OFFLINE_COLOR + reporterOfflineName : reporter.getDisplayName(),
					(target == null) ? Chat.OFFLINE_COLOR + targetOfflineName : target.getDisplayName(), reason);
		} else {
			staff.sendMessage("report.broadcast",
					(reporter == null) ? Chat.OFFLINE_COLOR + reporterOfflineName : reporter.getDisplayName(),
					(target == null) ? Chat.OFFLINE_COLOR + targetOfflineName : target.getDisplayName(), reason);
		}
		staff.playSound(Sound.ENDERMAN_SCREAM, 1, 1);
	}

}

/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.messaging;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.bukkit.models.Report;
import network.stratus.api.messaging.MessageProcessor;

/**
 * Processes a message which contains a {@link Report} by broadcasting it.
 * 
 * @author Ian Ballingall
 *
 */
public class ReportMessageProcessor implements MessageProcessor<Report> {

	private String serverName;

	public ReportMessageProcessor(String serverName) {
		this.serverName = serverName;
	}

	@Override
	public void process(Report report) {
		if (!report.getServerName().equals(serverName)) {
			StratusAPI plugin = StratusAPI.get();
			new BukkitRunnable() {
				@Override
				public void run() {
					Player reporter = plugin.getServer().getPlayer(report.getReporter());
					Player target = plugin.getServer().getPlayer(report.getTarget());
					report.broadcast(reporter, target, true);
				}
			}.runTask(plugin);

			SingleAudience.CONSOLE.sendMessage("report.broadcast.server", report.getServerName(),
					report.getReporterOfflineName(), report.getTargetOfflineName(), report.getReason());
		}
	}

}

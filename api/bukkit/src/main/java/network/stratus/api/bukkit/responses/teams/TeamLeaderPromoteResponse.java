/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.responses.teams;

import java.util.UUID;

/**
 * A response to a request to change a team's leader.
 * 
 * @author Ian Ballingall
 *
 */
public class TeamLeaderPromoteResponse {

	private UUID target;
	private String targetOfflineName;
	private String team;

	public UUID getTarget() {
		return target;
	}

	public String getTargetOfflineName() {
		return targetOfflineName;
	}

	public String getTeam() {
		return team;
	}

}

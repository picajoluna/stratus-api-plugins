/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.commands;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import app.ashcon.intake.Command;
import app.ashcon.intake.parametric.annotation.Default;
import app.ashcon.intake.parametric.annotation.Switch;
import app.ashcon.intake.parametric.annotation.Text;
import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.Chat;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.bukkit.models.TabCompletePlayer;
import network.stratus.api.bukkit.models.punishments.BukkitPunishmentFactory;
import network.stratus.api.bukkit.requests.IpBanCreateRequest;
import network.stratus.api.bukkit.requests.IpBanDeleteRequest;
import network.stratus.api.bukkit.requests.IpBanViewRequest;
import network.stratus.api.bukkit.requests.PunishmentCreateRequest;
import network.stratus.api.bukkit.requests.PunishmentLookupRequest;
import network.stratus.api.bukkit.requests.PunishmentModificationRequest;
import network.stratus.api.bukkit.responses.IpBanViewResponse;
import network.stratus.api.bukkit.responses.PunishmentLookupResponse;
import network.stratus.api.bukkit.util.Util;
import network.stratus.api.client.APIResponse;
import network.stratus.api.client.ResponseStatus;
import network.stratus.api.models.IpBan;
import network.stratus.api.models.punishment.Punishment;
import network.stratus.api.models.punishment.PunishmentFactory.Type;

/**
 * Commands for punishing players, viewing punishments and managing existing punishments.
 * 
 * @author Ian Ballingall
 *
 */
public class PunishmentCommands {
	
	@Command(aliases = { "punish", "p" },
			desc = "Punish a player, automatically determining the punishment type",
			perms = "stratusapi.command.punish",
			usage = "<player> <reason>")
	public void onPunish(CommandSender sender, TabCompletePlayer target, @Text String reason) {
		UUID issuer = (sender instanceof Player) ? ((Player) sender).getUniqueId() : null;
		String issuerName = (issuer == null) ? "CONSOLE" : ((Player) sender).getName();
		PunishmentCreateRequest request = new PunishmentCreateRequest(issuer, issuerName, target.getUsername(), reason, Type.AUTO);
		makeRequest(sender, request);
	}
	
	@Command(aliases = { "warn", "w" },
			desc = "Warn a player",
			perms = "stratusapi.command.warn",
			usage = "<player> <reason>")
	public void onWarn(CommandSender sender, TabCompletePlayer target, @Text String reason) {
		UUID issuer = (sender instanceof Player) ? ((Player) sender).getUniqueId() : null;
		String issuerName = (issuer == null) ? "CONSOLE" : ((Player) sender).getName();
		PunishmentCreateRequest request = new PunishmentCreateRequest(issuer, issuerName, target.getUsername(), reason, Type.WARN);
		makeRequest(sender, request);
	}
	
	@Command(aliases = { "kick", "k" },
			desc = "Kick a player",
			perms = "stratusapi.command.kick",
			usage = "<player> <reason>")
	public void onKick(CommandSender sender, TabCompletePlayer target, @Text String reason) {
		UUID issuer = (sender instanceof Player) ? ((Player) sender).getUniqueId() : null;
		String issuerName = (issuer == null) ? "CONSOLE" : ((Player) sender).getName();
		PunishmentCreateRequest request = new PunishmentCreateRequest(issuer, issuerName, target.getUsername(), reason, Type.KICK);
		makeRequest(sender, request);
	}
	
	@Command(aliases = { "tempban", "tb" },
			desc = "Temporarily ban a player for the specified duration",
			perms = "stratusapi.command.tempban",
			usage = "<player> <duration> <reason>")
	public void onTempban(CommandSender sender, TabCompletePlayer target, String duration, @Text String reason) {
		UUID issuer = (sender instanceof Player) ? ((Player) sender).getUniqueId() : null;
		String issuerName = (issuer == null) ? "CONSOLE" : ((Player) sender).getName();
		PunishmentCreateRequest request = new PunishmentCreateRequest(issuer, issuerName, target.getUsername(), reason, duration, Type.BAN);
		makeRequest(sender, request);
	}
	
	@Command(aliases = { "permaban", "pb" },
			desc = "Permanently ban a player",
			perms = "stratusapi.command.permaban",
			usage = "<player> <reason>")
	public void onPermaban(CommandSender sender, TabCompletePlayer target, @Text String reason) {
		UUID issuer = (sender instanceof Player) ? ((Player) sender).getUniqueId() : null;
		String issuerName = (issuer == null) ? "CONSOLE" : ((Player) sender).getName();
		PunishmentCreateRequest request = new PunishmentCreateRequest(issuer, issuerName, target.getUsername(), reason, Type.BAN);
		makeRequest(sender, request);
	}
	
	@Command(aliases = { "lookup" , "l" },
			desc = "Lookup a player's infractions",
			perms = "stratusapi.command.lookup",
			usage = "<player>")
	public void onLookup(CommandSender sender, TabCompletePlayer target) {
		Type[] types;
		if (sender instanceof ConsoleCommandSender || sender.hasPermission("stratusapi.punishments.warn.see.others")
				|| (sender.getName().equalsIgnoreCase(target.getUsername()))) {
			types = new Type[] { Type.WARN, Type.KICK, Type.BAN, Type.MUTE };
		} else {
			types = new Type[] { Type.KICK, Type.BAN, Type.MUTE };
		}
		
		StratusAPI.get().newSharedChain("punishments").<APIResponse<PunishmentLookupResponse>>asyncFirst(() -> {
			return new PunishmentLookupRequest(target.getUsername(), types).makeRequest(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			SingleAudience audience = new SingleAudience(sender);
			if (response.getStatus() == ResponseStatus.SUCCESS) {
				Player targetPlayer = StratusAPI.get().getServer().getPlayer(
						response.getResponseObject().getTarget());
				String targetDisplay = (targetPlayer == null) ? Chat.OFFLINE_COLOR
						+ response.getResponseObject().getTargetOfflineName() : targetPlayer.getDisplayName();

				List<BukkitPunishmentFactory> list = response.getResponseObject().getList();
				if (list.isEmpty()) {
					audience.sendMessage("punishment.lookup.empty", targetDisplay);
				} else {
					audience.sendMessage("punishment.lookup.title", targetDisplay);
					for (BukkitPunishmentFactory bpf : list) {
						Punishment p = bpf.getObject();
						String type = (p.hasExpired() ? ChatColor.STRIKETHROUGH : "")
								+ p.getType().getType().toLowerCase() + ChatColor.RESET;
						audience.sendMessage("punishment.lookup.entry", p.getNumber(), StratusAPI.get().getTranslator()
								.getTimeDifferenceString(sender.getLocale(), p.getTime(), new Date()),
								p.getIssuerOfflineName(), type, p.getReason());
					}
				}
			} else {
				audience.sendMessage("punishment.lookup.failure", response.getDescription());
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}
	
	@Command(aliases = "appeal",
			desc = "Mark a punishment as invalid",
			perms = "stratusapi.command.appeal",
			usage = "<player> <punishment number>")
	public void onAppeal(CommandSender sender, TabCompletePlayer target, int number) {
		UUID issuer = (sender instanceof Player) ? ((Player) sender).getUniqueId() : null;
		PunishmentModificationRequest request = new PunishmentModificationRequest(issuer, target.getUsername(), number,
				sender.hasPermission("stratusapi.command.appeal.others"), PunishmentModificationRequest.Action.APPEAL, null);

		StratusAPI.get().newSharedChain("punishments").<APIResponse<Void>>asyncFirst(() -> {
			return request.makeRequest(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			SingleAudience audience = new SingleAudience(sender);
			if (response.getStatus() == ResponseStatus.SUCCESS) {
				audience.sendMessage("punishment.appeal.success");
			} else {
				audience.sendMessage("punishment.appeal.failure", response.getDescription());
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}
	
	@Command(aliases = "expire",
			desc = "Expire a punishment immediately or set its new expiry",
			perms = "stratusapi.command.expire",
			usage = "<player> <punishment number> [<new duration>]")
	public void onExpire(CommandSender sender, TabCompletePlayer target, int number, @Default("") String duration) {
		UUID issuer = (sender instanceof Player) ? ((Player) sender).getUniqueId() : null;
		PunishmentModificationRequest request = new PunishmentModificationRequest(issuer, target.getUsername(), number,
				sender.hasPermission("stratusapi.command.expire.others"), PunishmentModificationRequest.Action.EXPIRE, duration);

		StratusAPI.get().newSharedChain("punishments").<APIResponse<Void>>asyncFirst(() -> {
			return request.makeRequest(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			SingleAudience audience = new SingleAudience(sender);
			if (response.getStatus() == ResponseStatus.SUCCESS) {
				audience.sendMessage("punishment.expire.success");
			} else {
				audience.sendMessage("punishment.expire.failure", response.getDescription());
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}
	
	@Command(aliases = "setreason",
			desc = "Set a new reason for the given punishment",
			perms = "stratusapi.command.reason",
			usage = "<player> <punishment number> <new reason>")
	public void onEditReason(CommandSender sender, TabCompletePlayer target, int number, @Text String reason) {
		UUID issuer = (sender instanceof Player) ? ((Player) sender).getUniqueId() : null;
		PunishmentModificationRequest request = new PunishmentModificationRequest(issuer, target.getUsername(), number,
				sender.hasPermission("stratusapi.command.reason.others"), PunishmentModificationRequest.Action.REASON, reason);

		StratusAPI.get().newSharedChain("punishments").<APIResponse<Void>>asyncFirst(() -> {
			return request.makeRequest(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			SingleAudience audience = new SingleAudience(sender);
			if (response.getStatus() == ResponseStatus.SUCCESS) {
				audience.sendMessage("punishment.reason.success");
			} else {
				audience.sendMessage("punishment.reason.failure", response.getDescription());
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = "settype",
			desc = "Change the type of a punishment",
			perms = "stratusapi.command.type",
			usage = "<player> <punishment number> <new type>")
	public void onSetType(CommandSender sender, TabCompletePlayer target, int number, String type) {
		UUID issuer = (sender instanceof Player) ? ((Player) sender).getUniqueId() : null;
		PunishmentModificationRequest request = new PunishmentModificationRequest(issuer, target.getUsername(), number,
				sender.hasPermission("stratusapi.command.type.others"), PunishmentModificationRequest.Action.TYPE, type.toUpperCase());

		StratusAPI.get().newSharedChain("punishments").<APIResponse<Void>>asyncFirst(() -> {
			return request.makeRequest(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			SingleAudience audience = new SingleAudience(sender);
			if (response.getStatus() == ResponseStatus.SUCCESS) {
				audience.sendMessage("punishment.type.success");
			} else {
				audience.sendMessage("punishment.type.failure", response.getDescription());
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = { "mute", "m" },
			desc = "Mute a player",
			perms = "stratusapi.command.mute",
			usage = "[-t duration] <player> <reason>")
	public void onMute(CommandSender sender, TabCompletePlayer target, @Text String reason, @Switch('t') String duration) {
		UUID issuer = (sender instanceof Player) ? ((Player) sender).getUniqueId() : null;
		String issuerName = (issuer == null) ? "CONSOLE" : ((Player) sender).getName();
		PunishmentCreateRequest request = new PunishmentCreateRequest(issuer, issuerName, target.getUsername(), reason, duration, Type.MUTE);
		makeRequest(sender, request);
	}

	@Command(aliases = { "ban-ip", "banip", "ip-ban", "ipban" },
			desc = "Ban an IP address",
			perms = "stratusapi.command.ipban",
			usage = "<IP address> [-e <end IP>] <description>")
	public void onIpBan(CommandSender sender, String ip, @Text String description, @Switch('e') String endIp) {
		StratusAPI.get().newSharedChain("ipban").<APIResponse<Void>>asyncFirst(() -> {
				return new IpBanCreateRequest(ip, endIp, description).makeRequest(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			SingleAudience audience = new SingleAudience(sender);
			if (response.getStatus() == ResponseStatus.SUCCESS) {
				audience.sendMessage("punishment.ipban.success", response.getDescription());
			} else {
				audience.sendMessage("punishment.failure", response.getDescription());
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = { "check-ip", "checkip" },
			desc = "Check an IP address to see if it is banned",
			perms = "stratusapi.command.checkip",
			usage = "<IP address>")
	public void onCheckIpBan(CommandSender sender, String ip) {
		StratusAPI.get().newSharedChain("ipban").<APIResponse<IpBanViewResponse>>asyncFirst(() -> {
			return new IpBanViewRequest(ip).makeRequest(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			SingleAudience audience = new SingleAudience(sender);
			if (response.getStatus() == ResponseStatus.SUCCESS) {
				IpBanViewResponse ipBanList = response.getResponseObject();
				for (IpBan ipBan : ipBanList.getIpBans()) {
					audience.sendMessage("punishment.ipban.view.1", ipBan.getBannedIp());
					audience.sendMessage("punishment.ipban.view.2", ipBan.get_id());
					audience.sendMessage("punishment.ipban.view.3", ipBan.getTime());
					audience.sendMessage("punishment.ipban.view.4", ipBan.getDescription());
				}
			} else {
				audience.sendMessage("punishment.ipban.view.failure", response.getDescription());
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = { "unban-ip", "unbanip" },
			desc = "Remove an IP ban",
			perms = "stratusapi.command.unbanip",
			usage = "<IP ban object ID>")
	public void onUnbanIp(CommandSender sender, String objectId) {
		StratusAPI.get().newSharedChain("ipban").<APIResponse<Void>>asyncFirst(() -> {
			return new IpBanDeleteRequest(objectId).makeRequest(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			SingleAudience audience = new SingleAudience(sender);
			if (response.getStatus() == ResponseStatus.SUCCESS) {
				audience.sendMessage("punishment.ipban.delete.success");
			} else {
				audience.sendMessage("punishment.ipban.delete.failure", response.getDescription());
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	/**
	 * Encapsulates the PunishmentCreateRequest.makeRequest() call inside a TaskChain,
	 * as the request logic is uniform across all commands.
	 * 
	 * @param sender The sender of the punishment command
	 * @param request The punishment request object
	 */
	private void makeRequest(CommandSender sender, PunishmentCreateRequest request) {
		StratusAPI.get().newSharedChain("punishments").<APIResponse<BukkitPunishmentFactory>>asyncFirst(() -> {
			return request.makeRequest(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			if (response.getStatus() == ResponseStatus.SUCCESS) {
				response.getResponseObject().getObject().enforce(false);
			} else {
				new SingleAudience(sender).sendMessage("punishment.failure", response.getDescription());
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

}

/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.ranked;

import javax.annotation.Nullable;

import org.bukkit.plugin.java.JavaPlugin;

import app.ashcon.intake.bukkit.BukkitIntake;
import app.ashcon.intake.bukkit.graph.BasicBukkitCommandGraph;
import app.ashcon.intake.fluent.DispatcherNode;
import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.commands.modules.StratusBukkitCommandModule;
import network.stratus.api.pgm.StratusAPIPGM;
import network.stratus.api.pgm.commands.ParticipantCommands;
import network.stratus.api.ranked.chat.RankedDisplayNameManager;
import network.stratus.api.ranked.commands.RankedCommands;
import network.stratus.api.ranked.match.ParticipateInMatchManager;
import network.stratus.api.ranked.match.RankedMatchListener;
import tc.oc.pgm.api.PGM;

/**
 * Extensions to the Stratus API for Ranked gameplay. This requires PGM extensions.
 * Provides participant management implementations for Ranked.
 * 
 * @author Ian Ballingall
 *
 */
public class StratusAPIRanked extends JavaPlugin {

	private static StratusAPIRanked plugin;

	@Override
	public void onEnable() {
		plugin = this;

		saveDefaultConfig();

		// Set up ranked prefix manager
		String hostPrefix = getConfig().getString("hostPrefix", "");
		String hostGroupName = getConfig().getString("hostGroupName", "");
		if (!hostPrefix.isEmpty() && !hostGroupName.isEmpty()) {
			RankedDisplayNameManager displayManager = new RankedDisplayNameManager(hostPrefix, hostGroupName);
			StratusAPI.get().setDisplayNameManager(displayManager);
			PGM.get().getPrefixRegistry().setPrefixProvider(displayManager);
		}

		ParticipateInMatchManager participantManager = new ParticipateInMatchManager(this);
		StratusAPIPGM.get().setParticipantManager(participantManager);
		getServer().getPluginManager().registerEvents(new RankedMatchListener(getConfig().getString("discordUrl", "")),
				this);

		BasicBukkitCommandGraph cmdGraph = new BasicBukkitCommandGraph(new StratusBukkitCommandModule());
		DispatcherNode root = cmdGraph.getRootDispatcherNode();
		root.registerCommands(new RankedCommands());
		root.registerNode("participant").registerCommands(new ParticipantCommands());
		new BukkitIntake(this, cmdGraph).register();

		getLogger().info("Stratus API Ranked extensions enabled");
	}

	@Override
	public void onDisable() {
		plugin = null;
		getLogger().info("Stratus API Ranked extensions disabled");
	}

	/**
	 * Get the current Stratus API plugin instance. If the plugin is not loaded,
	 * this will return null.
	 * 
	 * @return The plugin instance object
	 */
	@Nullable
	public static StratusAPIRanked get() {
		return plugin;
	}

}

/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.ranked.chat;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.pgm.chat.PGMDisplayNameManager;

/**
 * Manages display names on Ranked servers. If the player has the host group,
 * their prefixes will be overwritten by the host prefix. Otherwise, their
 * default prefixes are applied.
 * 
 * @author Ian Ballingall
 *
 */
public class RankedDisplayNameManager extends PGMDisplayNameManager {

	private final String hostPrefix;
	private final String hostGroupName;

	public RankedDisplayNameManager(String hostPrefix, String hostGroupName) {
		this.hostPrefix = ChatColor.translateAlternateColorCodes('`', hostPrefix);
		this.hostGroupName = hostGroupName;
	}

	@Override
	public void setDisplayName(Player player) {
		boolean isHost = StratusAPI.get().getPermissionsManager().getGroupsManager()
				.getPlayerGroups(player.getUniqueId()).parallelStream()
				.anyMatch(group -> group.getName().equals(hostGroupName));
		if (isHost) {
			setPrefix(player.getUniqueId(), hostPrefix);
		} else {
			super.setDisplayName(player);
		}
	}

}

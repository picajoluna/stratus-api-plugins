/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.velocity.chat;

import java.util.Locale;

import com.velocitypowered.api.command.CommandSource;
import com.velocitypowered.api.proxy.Player;

import net.kyori.text.TextComponent;
import network.stratus.api.chat.Audience;
import network.stratus.api.i18n.Translation;
import network.stratus.api.velocity.StratusAPIVelocity;

/**
 * Represents an {@link Audience} of one target.
 * 
 * @author Ian Ballingall
 *
 */
public class SingleAudience implements Audience {

	private CommandSource target;
	private Locale locale;

	public SingleAudience(CommandSource target) {
		this.target = target;

		// Only players have Locales, so we need to handle this accordingly
		if (target instanceof Player) {
			this.locale = ((Player) target).getPlayerSettings().getLocale();
		} else {
			this.locale = StratusAPIVelocity.get().getTranslator().getDefaultLocale();
		}
	}

	@Override
	public void sendMessage(Translation translation) {
		target.sendMessage(TextComponent.of(translation.translate(StratusAPIVelocity.get().getTranslator(), locale)));
	}

	@Override
	public void sendMessage(String stringKey) {
		target.sendMessage(
				TextComponent.of(StratusAPIVelocity.get().getTranslator().getStringOrDefaultLocale(locale, stringKey)));
	}

	@Override
	public void sendMessage(String stringKey, Object... args) {
		String message = StratusAPIVelocity.get().getTranslator().getStringOrDefaultLocale(locale, stringKey);
		target.sendMessage(TextComponent.of(String.format(message, args)));
	}

	@Override
	public void sendMessageRaw(String message) {
		target.sendMessage(TextComponent.of(message));
	}

	/**
	 * The {@link Audience} consisting solely of the console.
	 */
	public static final SingleAudience CONSOLE = new SingleAudience(
			StratusAPIVelocity.get().getServer().getConsoleCommandSource());

}

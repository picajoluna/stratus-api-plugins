/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.velocity.chat;

import network.stratus.api.velocity.StratusAPIVelocity;

/**
 * Represents an {@link Audience} consisting of all players online on this proxy
 * at the time of instantiation.
 * 
 * @author Ian Ballingall
 * 
 * @deprecated Unnecessary extra class. Use
 *             {@link MultiAudience.Builder#global()} instead. This will be
 *             removed as part of plugin version 7.0.0.
 */
@Deprecated
public class GlobalAudience extends MultiAudience {

	public GlobalAudience() {
		super(StratusAPIVelocity.get().getServer().getAllPlayers());
	}

}

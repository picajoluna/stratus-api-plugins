/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.velocity.listeners;

import java.util.Date;

import com.velocitypowered.api.event.ResultedEvent.ComponentResult;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.connection.LoginEvent;
import com.velocitypowered.api.proxy.Player;

import net.kyori.text.TextComponent;
import network.stratus.api.client.APIResponse;
import network.stratus.api.i18n.Translator;
import network.stratus.api.models.punishment.Punishment;
import network.stratus.api.velocity.StratusAPIVelocity;
import network.stratus.api.velocity.requests.LoginRequest;
import network.stratus.api.velocity.responses.LoginResponse;

/**
 * Listens on relevant login events and prohibits banned players and IPs from
 * joining.
 * 
 * @author Ian Ballingall
 *
 */
public class LoginListener {

	@Subscribe
	public void onLogin(LoginEvent event) {
		Translator translator = StratusAPIVelocity.get().getTranslator();

		Player player = event.getPlayer();
		LoginRequest request = new LoginRequest(player.getUniqueId(), player.getUsername(),
				player.getRemoteAddress().getAddress().getHostAddress());
		APIResponse<LoginResponse> response = request.makeRequest(StratusAPIVelocity.get().getApiClient());

		LoginResponse responseObject = response.getResponseObject();
		if (responseObject.getPunishment() != null) {
			Punishment punishment = responseObject.getPunishment().getObject();
			String message = "";
			if (punishment.getExpiry() == null) {
				message = String.format(translator.getStringOrDefaultLocale(player.getPlayerSettings().getLocale(),
						"punishment.ban.permanent.message"), punishment.getReason());
			} else {
				message = String.format(
						translator.getStringOrDefaultLocale(player.getPlayerSettings().getLocale(),
								"punishment.ban.temporary.message"),
						punishment.getReason(), translator.getTimeDifferenceString(
								player.getPlayerSettings().getLocale(), punishment.getExpiry(), new Date()));
			}

			event.setResult(ComponentResult.denied(TextComponent.of(message)));
		} else if (responseObject.isIpBanned()) {
			String message = translator.getString(player.getPlayerSettings().getLocale(), "punishment.ipban.message")
					.get();
			event.setResult(ComponentResult.denied(TextComponent.of(message)));
		}
	}

}

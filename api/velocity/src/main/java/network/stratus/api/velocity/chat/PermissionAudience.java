/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.velocity.chat;

import java.util.List;
import java.util.stream.Collectors;

import com.velocitypowered.api.command.CommandSource;

import network.stratus.api.velocity.StratusAPIVelocity;

/**
 * An {@link Audience} of all players who have a given permission node.
 * 
 * @author Ian Ballingall
 *
 */
public class PermissionAudience extends MultiAudience {

	private String permissionNode;

	public PermissionAudience(String permissionNode) {
		super(createTargets(permissionNode));
		this.permissionNode = permissionNode;
	}

	public String getPermissionNode() {
		return permissionNode;
	}

	private static List<CommandSource> createTargets(String node) {
		return StratusAPIVelocity.get().getServer().getAllPlayers().parallelStream()
				.filter(player -> player.hasPermission(node)).collect(Collectors.toList());
	}

}

/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.velocity.models.punishments;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import com.velocitypowered.api.proxy.Player;

import network.stratus.api.models.punishment.Punishment;
import network.stratus.api.velocity.StratusAPIVelocity;

/**
 * Abstraction representing of a {@link Punishment}, optionally encapsulated the
 * {@link Player}s who issued and receive the punishment.
 * 
 * @author Ian Ballingall
 *
 */
public abstract class VelocityPunishment extends Punishment {

	protected Optional<Player> issuerPlayer;
	protected Optional<Player> targetPlayer;

	public VelocityPunishment(String _id, UUID issuer, String issuerOfflineName, UUID target, String targetOfflineName,
			Date time, Date expiry, String reason, boolean active, int number, String serverName) {
		super(_id, issuer, issuerOfflineName, target, targetOfflineName, time, expiry, reason, active, number,
				serverName);
	}

	@Override
	public Punishment initialisePlayers() {
		if (issuer != null) {
			issuerPlayer = StratusAPIVelocity.get().getServer().getPlayer(issuer);
		}

		targetPlayer = StratusAPIVelocity.get().getServer().getPlayer(issuer);

		return this;
	}

	@Override
	public Punishment formatNames() {
		// No format is presently defined for this implementation
		return this;
	}

}

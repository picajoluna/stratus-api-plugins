/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.velocity.requests;

import java.util.UUID;

import network.stratus.api.client.APIClient;
import network.stratus.api.client.APIRequest;
import network.stratus.api.client.APIResponse;
import network.stratus.api.requests.AbstractPlayerRequest;
import network.stratus.api.velocity.responses.LoginResponse;

/**
 * A request triggered with a player attempts to log in. The player's persistent
 * data is updated, and any active ban or IP ban is retrieved. Group and mute
 * information is also available.
 * 
 * @author Ian Ballingall
 *
 */
public class LoginRequest extends AbstractPlayerRequest implements APIRequest<LoginResponse> {

	public LoginRequest(UUID uuid, String username, String ip) {
		super(uuid, username, ip);
	}

	@Override
	public String getEndpoint() {
		return "/players/login";
	}

	@Override
	public Class<LoginResponse> getResponseType() {
		return LoginResponse.class;
	}

	@Override
	public APIResponse<LoginResponse> makeRequest(APIClient client) {
		return client.post(this);
	}

}

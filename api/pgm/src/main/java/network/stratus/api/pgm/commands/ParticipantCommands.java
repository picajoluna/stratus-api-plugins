/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import app.ashcon.intake.Command;
import app.ashcon.intake.parametric.annotation.Text;
import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.Chat;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.bukkit.models.TabCompletePlayer;
import network.stratus.api.bukkit.requests.FindPlayerRequest;
import network.stratus.api.bukkit.responses.FindPlayerResponse;
import network.stratus.api.bukkit.util.Util;
import network.stratus.api.client.APIResponse;
import network.stratus.api.client.ResponseStatus;
import network.stratus.api.pgm.StratusAPIPGM;
import network.stratus.api.pgm.match.ParticipantManager;
import tc.oc.pgm.api.PGM;
import tc.oc.pgm.api.match.Match;
import tc.oc.pgm.api.party.Party;
import tc.oc.pgm.api.player.MatchPlayer;
import tc.oc.pgm.teams.TeamMatchModule;

/**
 * Commands for managing a player's participation in a match.
 * 
 * @author Ian Ballingall
 *
 */
public class ParticipantCommands {

	@Command(aliases = "register",
			desc = "Register a player to a team",
			usage = "<player> <team>",
			perms = "stratusapi.command.participant.register")
	public void onRegisterParticipant(CommandSender sender, TabCompletePlayer input, @Text String team) {
		Player player = input.getPlayer().orElse(null);

		StratusAPI.get().newSharedChain("participation").<APIResponse<FindPlayerResponse>>asyncFirst(() -> {
			if (player == null) {
				return new FindPlayerRequest(input.getUsername()).makeRequest(StratusAPI.get().getApiClient());
			} else {
				return null;
			}
		}).<UUID>sync(response -> {
			if (response == null) {
				return player.getUniqueId();
			} else {
				if (response.getStatus() == ResponseStatus.SUCCESS && response.getResponseObject().getUuid() != null) {
					return response.getResponseObject().getUuid();
				} else {
					new SingleAudience(sender).sendMessage("team.register.failure", response.getDescription());
					return null;
				}
			}
		}).abortIfNull().syncLast(uuid -> {
			ParticipantManager manager = StratusAPIPGM.get().getParticipantManager();
			SingleAudience audience = new SingleAudience(sender);
			if (manager.isPlayerRegistered(PGM.get().getMatchManager().getMatch(sender), uuid)) {
				audience.sendMessage("team.register.failure.alreadyregistered");
				return;
			}

			Match match = PGM.get().getMatchManager().getMatch(sender);
			TeamMatchModule tmm = match.getModule(TeamMatchModule.class);

			if (team.trim().toLowerCase().startsWith("obs")) {
				audience.sendMessage("team.register.failure.obs");
				return;
			} else {
				Party party = tmm.bestFuzzyMatch(team.trim());
				if (party == null) {
					audience.sendMessage("team.register.failure.doesnotexist");
				} else {
					StratusAPIPGM.get().getParticipantManager().registerParticipant(match, uuid, party);
					audience.sendMessage("team.register.success", party.getColoredName());

					if (player != null) {
						MatchPlayer matchPlayer = match.getPlayer((Player) player);
						match.setParty(matchPlayer, party);
					}
				}
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = "unregister",
			desc = "Unregisters a player from a team",
			usage = "<player>",
			perms = "stratusapi.command.participant.unregister")
	public void onUnregisterParticipant(CommandSender sender, TabCompletePlayer input) {
		Player player = input.getPlayer().orElse(null);

		StratusAPI.get().newSharedChain("participation").<APIResponse<FindPlayerResponse>>asyncFirst(() -> {
			if (player == null) {
				return new FindPlayerRequest(input.getUsername()).makeRequest(StratusAPI.get().getApiClient());
			} else {
				return null;
			}
		}).<UUID>sync(response -> {
			if (response == null) {
				return player.getUniqueId();
			} else {
				if (response.getStatus() == ResponseStatus.SUCCESS && response.getResponseObject().getUuid() != null) {
					return response.getResponseObject().getUuid();
				} else {
					new SingleAudience(sender).sendMessage("team.unregister.failure", response.getDescription());
					return null;
				}
			}
		}).abortIfNull().syncLast(uuid -> {
			SingleAudience audience = new SingleAudience(sender);
			Match match = PGM.get().getMatchManager().getMatch(sender);
			ParticipantManager manager = StratusAPIPGM.get().getParticipantManager();

			if (manager.isPlayerRegistered(match, uuid)) {
				manager.unregisterParticipant(match, uuid);
				audience.sendMessage("team.unregister.success");

				if (player != null) {
					MatchPlayer matchPlayer = match.getPlayer((Player) player);
					match.setParty(matchPlayer, match.getDefaultParty());
				}
			} else {
				audience.sendMessage("team.unregister.failure.notregistered");
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = "list",
			desc = "See all registered participants",
			perms = "stratusapi.command.participant.unregister")
	public void onListParticipants(CommandSender sender) {
		ParticipantManager manager = StratusAPIPGM.get().getParticipantManager();
		Map<UUID, Party> participants = manager.getParticipants(PGM.get().getMatchManager().getMatch(sender));
		Map<Party, List<UUID>> teams = new HashMap<>();
		participants.entrySet().forEach(e -> {
			if (!teams.containsKey(e.getValue())) {
				teams.put(e.getValue(), new ArrayList<>());
			}
			
			teams.get(e.getValue()).add(e.getKey());
		});

		SingleAudience audience = new SingleAudience(sender);
		teams.entrySet().forEach(e -> {
			audience.sendMessageRaw(e.getKey().getName() + ":");
			e.getValue().forEach(uuid -> {
				OfflinePlayer player = Bukkit.getOfflinePlayer(uuid);
				String name = null;
				if (player.getPlayer() == null) {
					if (player.getName() == null) {
						name = Chat.OFFLINE_COLOR + uuid.toString();
					} else {
						name = Chat.OFFLINE_COLOR + player.getName();
					}
				} else {
					name = player.getPlayer().getDisplayName();
				}

				audience.sendMessageRaw("- " + name);
			});
		});
	}

}

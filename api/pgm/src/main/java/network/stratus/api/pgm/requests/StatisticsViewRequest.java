/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.requests;

import java.util.UUID;

import network.stratus.api.client.APIClient;
import network.stratus.api.client.APIRequest;
import network.stratus.api.client.APIResponse;
import network.stratus.api.pgm.responses.StatisticsResponse;

/**
 * Represents a request to the API from a player wanting to see their statistics.
 * 
 * @author Ian Ballingall
 *
 */
public class StatisticsViewRequest implements APIRequest<StatisticsResponse> {

	private UUID uuid;

	public StatisticsViewRequest(UUID uuid) {
		this.uuid = uuid;
	}

	@Override
	public String getEndpoint() {
		return "/pgm/statistics/" + uuid.toString();
	}

	@Override
	public Class<StatisticsResponse> getResponseType() {
		return StatisticsResponse.class;
	}

	@Override
	public APIResponse<StatisticsResponse> makeRequest(APIClient client) {
		return client.get(this);
	}

}

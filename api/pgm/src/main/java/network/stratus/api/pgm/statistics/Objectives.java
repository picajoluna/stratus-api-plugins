/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.statistics;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Maps players' UUIDs to their objective counts for this match.
 * 
 * @author Ian Ballingall
 *
 */
public class Objectives {

	private Map<UUID, AtomicInteger> wools;
	private Map<UUID, AtomicInteger> monuments;
	private Map<UUID, AtomicInteger> cores;
	private Map<UUID, AtomicInteger> flags;
	/** Indicates whether the corresponding match is running. */
	@JsonIgnore
	private boolean matchActive;

	public Objectives() {
		this.wools = new HashMap<>();
		this.monuments = new HashMap<>();
		this.cores = new HashMap<>();
		this.flags = new HashMap<>();
		this.matchActive = true;
	}

	public Map<UUID, AtomicInteger> getWools() {
		return wools;
	}

	public Map<UUID, AtomicInteger> getMonuments() {
		return monuments;
	}

	public Map<UUID, AtomicInteger> getCores() {
		return cores;
	}

	public Map<UUID, AtomicInteger> getFlags() {
		return flags;
	}

	@JsonIgnore
	protected boolean isMatchActive() {
		return matchActive;
	}

	@JsonIgnore
	protected void setMatchActive(boolean matchActive) {
		this.matchActive = matchActive;
	}

	/**
	 * Get a player's wools in the current match by their UUID. This will return
	 * their current wool count if the match is ongoing, and 0 otherwise.
	 * 
	 * @param uuid The UUID of the player whose wools to find
	 * @return The number of wools the player has in this match
	 */
	@JsonIgnore
	public int getWools(UUID uuid) {
		return matchActive ? wools.getOrDefault(uuid, new AtomicInteger()).intValue() : 0;
	}

	/**
	 * Get a player's monuments in the current match by their UUID. This will return
	 * their current  count if the match is ongoing, and 0 otherwise.
	 * 
	 * @param uuid The UUID of the player whose monuments to find
	 * @return The number of monuments the player has in this match
	 */
	@JsonIgnore
	public int getMonuments(UUID uuid) {
		return matchActive ? monuments.getOrDefault(uuid, new AtomicInteger()).intValue() : 0;
	}

	/**
	 * Get a player's cores in the current match by their UUID. This will return
	 * their current core count if the match is ongoing, and 0 otherwise.
	 * 
	 * @param uuid The UUID of the player whose cores to find
	 * @return The number of cores the player has in this match
	 */
	@JsonIgnore
	public int getCores(UUID uuid) {
		return matchActive ? cores.getOrDefault(uuid, new AtomicInteger()).intValue() : 0;
	}

	/**
	 * Get a player's flags in the current match by their UUID. This will return
	 * their current flag count if the match is ongoing, and 0 otherwise.
	 * 
	 * @param uuid The UUID of the player whose flags to find
	 * @return The number of flags the player has in this match
	 */
	@JsonIgnore
	public int getFlags(UUID uuid) {
		return matchActive ? flags.getOrDefault(uuid, new AtomicInteger()).intValue() : 0;
	}

}

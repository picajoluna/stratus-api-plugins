/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.requests;

import network.stratus.api.client.APIClient;
import network.stratus.api.client.APIRequest;
import network.stratus.api.client.APIResponse;
import network.stratus.api.pgm.responses.CustomKitViewResponse;

import java.util.UUID;

/**
 * Represents a request to the API for fetching a player's custom kits
 * 
 * @author ShinyDialga
 *
 */
public class CustomKitViewRequest implements APIRequest<CustomKitViewResponse> {

	private UUID uuid;

	public CustomKitViewRequest(UUID uuid) {
		this.uuid = uuid;
	}

	public UUID getUuid() {
		return uuid;
	}

	@Override
	public String getEndpoint() {
		return "/pgm/customkit/view/" + uuid;
	}

	@Override
	public Class<CustomKitViewResponse> getResponseType() {
		return CustomKitViewResponse.class;
	}

	@Override
	public APIResponse<CustomKitViewResponse> makeRequest(APIClient client) {
		return client.get(this);
	}

}

/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.commands;

import java.util.UUID;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import app.ashcon.intake.Command;
import app.ashcon.intake.bukkit.parametric.annotation.Sender;
import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.bukkit.util.Util;
import network.stratus.api.client.APIResponse;
import network.stratus.api.client.ResponseStatus;
import network.stratus.api.pgm.StratusAPIPGM;
import network.stratus.api.pgm.requests.StatisticsViewRequest;
import network.stratus.api.pgm.responses.StatisticsResponse;
import network.stratus.api.pgm.statistics.MatchStatistics;
import network.stratus.api.pgm.statistics.Objectives;

/**
 * Commands for viewing statistics.
 * 
 * @author Ian Ballingall
 *
 */
public class StatisticsCommands {

	@Command(aliases = { "statistics", "stats" },
			desc = "View your statistics")
	public void viewStatistics(@Sender CommandSender sender) {
		// We can assume this is a player because @Sender enforces it at command level
		StatisticsViewRequest request = new StatisticsViewRequest(((Player) sender).getUniqueId());

		StratusAPI.get().newSharedChain("statistics").<APIResponse<StatisticsResponse>>asyncFirst(() -> {
			return request.makeRequest(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			SingleAudience audience = new SingleAudience(sender);
			if (response.getStatus() == ResponseStatus.SUCCESS) {
				// Remote statistics
				StatisticsResponse stats = response.getResponseObject();
				UUID uuid = stats.getUuid();

				// Current match statistics
				MatchStatistics matchStats = StratusAPIPGM.get().getStatisticsManager().getMatchStatistics();
				Objectives matchObjectives = matchStats.getObjectives();

				int kills = matchStats.getKills(uuid) + stats.getKills();
				int deaths = matchStats.getDeaths(uuid) + stats.getDeaths();

				audience.sendMessage("statistics.view.title.own");
				// If player has no deaths, KDR = kills to avoid divide by zero
				audience.sendMessage("statistics.view.killsdeaths", kills, deaths,
						(deaths == 0) ? (float) kills : (float) kills / deaths);

				audience.sendMessage("statistics.view.objectives.title");
				audience.sendMessage("statistics.view.objectives", matchObjectives.getWools(uuid) + stats.getWools(),
						matchObjectives.getCores(uuid) + stats.getCores(),
						matchObjectives.getMonuments(uuid) + stats.getMonuments(),
						matchObjectives.getFlags(uuid) + stats.getFlags());
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

}

/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.customkit;

import org.bukkit.entity.Player;

import network.stratus.api.pgm.responses.CustomKitResponse;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class CustomKitTracker {
    private Map<Player, Set<CustomKitResponse>> tracker;

    public CustomKitTracker() {
        this.tracker = new HashMap<>();
    }

    public void removePlayer(Player player) {
        tracker.remove(player);
    }

    public void addKit(Player player, CustomKitResponse kit) {
        Set<CustomKitResponse> kits = tracker.get(player);
        if (kits == null) {
            kits = new HashSet<>();
        }

        CustomKitResponse previousKit = getCustomKit(player, kit.getMapId(), kit.getMapVersion());
        if (previousKit != null) {
            previousKit.setSlots(kit.getSlots());
        } else {
            kits.add(kit);
        }

        tracker.put(player, kits);
    }

    public void removeKit(Player player, String mapId, String version) {
        CustomKitResponse previousKit = getCustomKit(player, mapId, version);
        if (previousKit != null) {
            Set<CustomKitResponse> kits = tracker.get(player);
            kits.remove(previousKit);
            tracker.put(player, kits);
        }
    }

    public Set<CustomKitResponse> getKits(Player player) {
        return tracker.get(player);
    }

    public CustomKitResponse getCustomKit(Player player, String mapId, String mapVersion) {
        Set<CustomKitResponse> kits = tracker.get(player);

        if (kits != null) {
            for (CustomKitResponse kit : kits) {
                if (kit.getMapId().equalsIgnoreCase(mapId) && kit.getMapVersion().equalsIgnoreCase(mapVersion)) {
                    return kit;
                }
            }
        }

        return null;
    }
}

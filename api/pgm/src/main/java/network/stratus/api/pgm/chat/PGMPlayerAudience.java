/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.chat;

import org.bukkit.entity.Player;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.SingleAudience;
import tc.oc.pgm.util.nms.NMSHacks;

/**
 * Represents an audience of a single player on a server running PGM. This makes
 * use of some PGM-provided APIs to perform certain actions.
 * 
 * @author Ian Ballingall
 *
 */
public class PGMPlayerAudience extends SingleAudience {

	private Player player;

	public PGMPlayerAudience(Player player) {
		super(player);
		this.player = player;
	}

	public void sendHotbarMessage(String stringKey) {
		String message = StratusAPI.get().getTranslator().getStringOrDefaultLocale(player.getLocale(), stringKey);
		NMSHacks.sendHotbarMessage(player, message);
	}

	public void sendHotbarMessage(String stringKey, Object... args) {
		String message = StratusAPI.get().getTranslator().getStringOrDefaultLocale(player.getLocale(), stringKey);
		NMSHacks.sendHotbarMessage(player, String.format(message, args));
	}

	public void sendHotbarMessageRaw(String message) {
		NMSHacks.sendHotbarMessage(player, message);
	}

}

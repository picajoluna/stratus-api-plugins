/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm;

import app.ashcon.intake.bukkit.BukkitIntake;
import app.ashcon.intake.bukkit.graph.BasicBukkitCommandGraph;
import app.ashcon.intake.fluent.DispatcherNode;
import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.pgm.chat.PGMDisplayNameManager;
import network.stratus.api.pgm.commands.CustomKitCommands;
import network.stratus.api.pgm.commands.ModerationCommands;
import network.stratus.api.pgm.commands.StatisticsCommands;
import network.stratus.api.pgm.customkit.CustomKitListener;
import network.stratus.api.pgm.customkit.CustomKitTracker;
import network.stratus.api.pgm.listeners.ChunkLoadCrashProtection;
import network.stratus.api.pgm.listeners.FlyGlitchProtection;
import network.stratus.api.pgm.listeners.FreezeListener;
import network.stratus.api.pgm.listeners.StatisticsListener;
import network.stratus.api.pgm.match.NullParticipantManager;
import network.stratus.api.pgm.match.NullReadyManager;
import network.stratus.api.pgm.match.ParticipantManager;
import network.stratus.api.pgm.match.ReadyManager;
import network.stratus.api.pgm.moderation.FreezeManager;
import network.stratus.api.pgm.statistics.StatisticsManager;
import network.stratus.api.pgm.tab.GroupPlayerOrder;

import javax.annotation.Nullable;

import org.bukkit.plugin.java.JavaPlugin;
import tc.oc.pgm.api.PGM;

/**
 * Extensions to the Stratus API which interact with PGM. This should be
 * deployed on PGM servers. Provides statistics and participant management
 * systems.
 * 
 * @author Ian Ballingall
 *
 */

public class StratusAPIPGM extends JavaPlugin {

	private static StratusAPIPGM plugin;

	private StatisticsManager statsManager;
	private ParticipantManager participantManager;
	private FreezeManager freezeManager;
	private ReadyManager readyManager;

	private CustomKitTracker tracker;

	@Override
	public void onEnable() {
		saveDefaultConfig();
		plugin = this;

		freezeManager = new FreezeManager();

		BasicBukkitCommandGraph cmdGraph = new BasicBukkitCommandGraph();
		DispatcherNode root = cmdGraph.getRootDispatcherNode();

		// If statistics enabled, activate related managers, listeners and commands
		if (getConfig().getBoolean("statistics.enabled", true)) {
			statsManager = new StatisticsManager();
			getServer().getPluginManager().registerEvents(new StatisticsListener(), this);
			root.registerCommands(new StatisticsCommands());
		}

		if (getConfig().getBoolean("customkits.enabled", true)) {
			tracker = new CustomKitTracker();
			getServer().getPluginManager().registerEvents(new CustomKitListener(), this);
			root.registerCommands(new CustomKitCommands());
		}

		root.registerCommands(new ModerationCommands(freezeManager));

		new BukkitIntake(this, cmdGraph).register();

		getServer().getPluginManager().registerEvents(new FlyGlitchProtection(), this);
		getServer().getPluginManager().registerEvents(new ChunkLoadCrashProtection(), this);
		getServer().getPluginManager().registerEvents(new FreezeListener(freezeManager), this);

		PGMDisplayNameManager displayManager = new PGMDisplayNameManager();
		StratusAPI.get().setDisplayNameManager(displayManager);
		PGM.get().getPrefixRegistry().setPrefixProvider(displayManager);

		PGM.get().getMatchTabManager().setPlayerOrderFactory(new GroupPlayerOrder.Factory());

		participantManager = new NullParticipantManager();
		readyManager = new NullReadyManager();

		getLogger().info("Stratus API PGM extensions enabled");
	}

	public void onDisable() {
		plugin = null;
		getLogger().info("Stratus API PGM extensions disabled");
	}

	/**
	 * Get the current plugin instance. If the plugin is not loaded, this will
	 * return null.
	 * 
	 * @return The plugin instance object
	 */
	@Nullable
	public static StratusAPIPGM get() {
		return plugin;
	}

	@Nullable
	public StatisticsManager getStatisticsManager() {
		return statsManager;
	}

	public ParticipantManager getParticipantManager() {
		return participantManager;
	}

	public void setParticipantManager(ParticipantManager manager) {
		participantManager = manager;
	}

	public FreezeManager getFreezeManager() {
		return freezeManager;
	}

	public ReadyManager getReadyManager() {
		return readyManager;
	}

	public void setReadyManager(ReadyManager manager) {
		readyManager = manager;
	}

	public CustomKitTracker getKitTracker() {
		return tracker;
	}
}

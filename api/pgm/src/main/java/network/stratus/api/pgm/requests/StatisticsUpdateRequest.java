/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.requests;

import network.stratus.api.client.APIClient;
import network.stratus.api.client.APIRequest;
import network.stratus.api.client.APIResponse;
import network.stratus.api.pgm.statistics.MatchStatistics;

/**
 * Represents a request to the API to batch update user's statistics from the
 * previous match.
 * 
 * @author Ian Ballingall
 *
 */
public class StatisticsUpdateRequest extends MatchStatistics implements APIRequest<Void> {

	public StatisticsUpdateRequest(MatchStatistics statistics) {
		super(statistics.getTrackedPlayers(), statistics.getKills(), statistics.getDeaths(), statistics.getObjectives(),
				statistics.getPlaytime(), false);
	}

	@Override
	public String getEndpoint() {
		return "/pgm/statistics";
	}

	@Override
	public Class<Void> getResponseType() {
		return Void.class;
	}

	@Override
	public APIResponse<Void> makeRequest(APIClient client) {
		return client.post(this);
	}

}

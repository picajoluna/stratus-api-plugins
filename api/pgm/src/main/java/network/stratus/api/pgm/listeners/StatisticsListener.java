/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.listeners;

import java.util.UUID;
import java.util.stream.Collectors;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.pgm.StratusAPIPGM;
import network.stratus.api.pgm.statistics.StatisticsManager;
import tc.oc.pgm.api.match.Match;
import tc.oc.pgm.api.match.event.MatchFinishEvent;
import tc.oc.pgm.api.match.event.MatchLoadEvent;
import tc.oc.pgm.api.match.event.MatchStartEvent;
import tc.oc.pgm.api.player.MatchPlayer;
import tc.oc.pgm.api.player.ParticipantState;
import tc.oc.pgm.api.player.event.MatchPlayerDeathEvent;
import tc.oc.pgm.core.CoreLeakEvent;
import tc.oc.pgm.destroyable.DestroyableContribution;
import tc.oc.pgm.destroyable.DestroyableDestroyedEvent;
import tc.oc.pgm.events.PlayerParticipationStartEvent;
import tc.oc.pgm.events.PlayerParticipationStopEvent;
import tc.oc.pgm.flag.event.FlagCaptureEvent;
import tc.oc.pgm.wool.PlayerWoolPlaceEvent;

/**
 * Listener methods pertaining to statistics. These listen to PGM events in
 * order to track player kills, deaths and objectives, and to know when matches
 * are starting and ending.
 * 
 * @author Ian Ballingall
 *
 */
public class StatisticsListener implements Listener {

	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerDeath(MatchPlayerDeathEvent event) {
		if (!event.isTeamKill()) {
			StratusAPIPGM.get().getStatisticsManager().registerDeath(event.getVictim().getId());
			StratusAPIPGM.get().getStatisticsManager().sendHotbarStats(event.getVictim().getId());

			if (event.getKiller() != null && !event.isSelfKill() && !event.isSuicide()) {
				StratusAPIPGM.get().getStatisticsManager().registerKill(event.getKiller().getId());
				StratusAPIPGM.get().getStatisticsManager().sendHotbarStats(event.getKiller().getId());
			}
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onCoreLeak(CoreLeakEvent event) {
		for (ParticipantState ps : event.getCore().getTouchingPlayers()) {
			StratusAPIPGM.get().getStatisticsManager().registerCore(ps.getId());
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onMonumentBreak(DestroyableDestroyedEvent event) {
		for (DestroyableContribution dc : event.getDestroyable().getContributions()) {
			StratusAPIPGM.get().getStatisticsManager().registerMonument(dc.getPlayerState().getId());
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onWoolCapture(PlayerWoolPlaceEvent event) {
		StratusAPIPGM.get().getStatisticsManager().registerWool(event.getPlayer().getId());
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onFlagCapture(FlagCaptureEvent event) {
		StratusAPIPGM.get().getStatisticsManager().registerFlag(event.getCarrier().getId());
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onMatchLoad(MatchLoadEvent event) {
		StratusAPIPGM.get().getStatisticsManager().newMatch();
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onMatchStart(MatchStartEvent event) {
		Match match = event.getMatch();
		for (MatchPlayer player : match.getParticipants()) {
			StratusAPIPGM.get().getStatisticsManager().newPlaytimeTask(player.getId(), match);
			StratusAPIPGM.get().getStatisticsManager().trackPlayers(
					event.getMatch().getParticipants().stream().map(MatchPlayer::getId).collect(Collectors.toList()));
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onMatchFinish(MatchFinishEvent event) {
		StatisticsManager statsManager = StratusAPIPGM.get().getStatisticsManager();

		// Delay by one tick to ensure the above events are handled before we display
		// statistics to players and submit them to the API
		StratusAPI.get().newSharedChain("statistics").delay(1).sync(() -> {
			event.getMatch().getParticipants().stream().map(MatchPlayer::getId)
					.forEach(statsManager::sendMatchStatisticsSummary);
		}).async(() -> {
			statsManager.endMatch().makeRequest(StratusAPI.get().getApiClient());
		}).execute();
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onLeaveMatch(PlayerParticipationStopEvent event) {
		// Only display statistics if the match is active and leave was not cancelled
		if (!event.isCancelled() && event.getMatch().isRunning()) {
			UUID uuid = event.getPlayer().getId();
			StatisticsManager statsManager = StratusAPIPGM.get().getStatisticsManager();
			statsManager.sendMatchStatisticsSummary(uuid);
			statsManager.removePlaytimeTask(uuid);
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onJoinMatch(PlayerParticipationStartEvent event) {
		if (!event.isCancelled() && event.getMatch().isRunning()) {
			StratusAPIPGM.get().getStatisticsManager().newPlaytimeTask(event.getPlayer().getId(), event.getMatch());
			StratusAPIPGM.get().getStatisticsManager().trackPlayer(event.getPlayer().getId());
		}
	}

}

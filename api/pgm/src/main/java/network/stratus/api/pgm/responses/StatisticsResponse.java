/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.responses;

import java.util.UUID;

/**
 * Represents a response to a request for a player's statistics.
 * 
 * @author Ian Ballingall
 *
 */
public class StatisticsResponse {

	private UUID uuid;
	private int kills;
	private int deaths;
	private int wools;
	private int monuments;
	private int cores;
	private int flags;
	private int playtime;

	public UUID getUuid() {
		return uuid;
	}

	public int getKills() {
		return kills;
	}

	public int getDeaths() {
		return deaths;
	}

	public int getWools() {
		return wools;
	}

	public int getMonuments() {
		return monuments;
	}

	public int getCores() {
		return cores;
	}

	public int getFlags() {
		return flags;
	}

	public int getPlaytime() {
		return playtime;
	}

}

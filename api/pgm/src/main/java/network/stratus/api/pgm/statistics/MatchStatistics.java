/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.statistics;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import com.fasterxml.jackson.annotation.JsonIgnore;

import network.stratus.api.pgm.statistics.Objectives;

/**
 * Tracks player statistics over the course of a
 * {@link tc.oc.pgm.api.match.Match}.
 * 
 * @author Ian Ballingall
 *
 */
public class MatchStatistics {

	private Set<UUID> trackedPlayers;
	private Map<UUID, AtomicInteger> kills;
	private Map<UUID, AtomicInteger> deaths;
	private Objectives objectives;
	private Map<UUID, AtomicInteger> playtime;
	/** Indicates whether the corresponding match is running. */
	@JsonIgnore
	private boolean matchActive;

	public MatchStatistics() {
		this.trackedPlayers = new HashSet<>();
		this.kills = new HashMap<>();
		this.deaths = new HashMap<>();
		this.objectives = new Objectives();
		this.playtime = new ConcurrentHashMap<>();
		this.matchActive = true;
	}

	public MatchStatistics(Set<UUID> trackedPlayers, Map<UUID, AtomicInteger> kills, Map<UUID, AtomicInteger> deaths,
			Objectives objectives, Map<UUID, AtomicInteger> playtime, boolean matchActive) {
		this.trackedPlayers = trackedPlayers;
		this.kills = kills;
		this.deaths = deaths;
		this.objectives = objectives;
		this.playtime = playtime;
		this.matchActive = matchActive;
	}

	public Set<UUID> getTrackedPlayers() {
		return trackedPlayers;
	}

	public Map<UUID, AtomicInteger> getKills() {
		return kills;
	}

	public Map<UUID, AtomicInteger> getDeaths() {
		return deaths;
	}

	public Objectives getObjectives() {
		return objectives;
	}

	public Map<UUID, AtomicInteger> getPlaytime() {
		return playtime;
	}

	@JsonIgnore
	protected boolean isMatchActive() {
		return matchActive;
	}

	@JsonIgnore
	protected void setMatchActive(boolean matchActive) {
		this.matchActive = matchActive;
		objectives.setMatchActive(matchActive);
	}

	/**
	 * Get a player's kills in the current match by their UUID. This will return
	 * their current kill count if the match is ongoing, and 0 otherwise.
	 * 
	 * @param uuid The UUID of the player whose kills to find
	 * @return The number of kills the player has in this match
	 */
	@JsonIgnore
	public int getKills(UUID uuid) {
		return matchActive ? kills.getOrDefault(uuid, new AtomicInteger()).intValue() : 0;
	}

	/**
	 * Get a player's deaths in the current match by their UUID. This will return
	 * their current death count if the match is ongoing, and 0 otherwise.
	 * 
	 * @param uuid The UUID of the player whose deaths to find
	 * @return The number of deaths the player has in this match
	 */
	@JsonIgnore
	public int getDeaths(UUID uuid) {
		return matchActive ? deaths.getOrDefault(uuid, new AtomicInteger()).intValue() : 0;
	}

	/**
	 * Get a player's playtime in the current match in minutes. by their UUID. This
	 * will return their current playtime if the match is running, and 0 otherwise.
	 * 
	 * @param uuid The UUID of the player whose playtime to find
	 * @return The player's playtime
	 */
	@JsonIgnore
	public int getPlaytime(UUID uuid) {
		return matchActive ? playtime.getOrDefault(uuid, new AtomicInteger()).intValue() : 0;
	}

}

/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.chat;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.DisplayNameManager;
import tc.oc.pgm.api.event.PrefixChangeEvent;
import tc.oc.pgm.api.prefix.PrefixProvider;

/**
 * Manages display names when using PGM. This acts as both the Stratus-API
 * display name manager and as the prefix provider for PGM. This will update the
 * stored prefix on request from the API, and provide the stored prefix to PGM
 * when requested.
 *
 * @author Ian Ballingall, Meeples10
 *
 */
public class PGMDisplayNameManager implements DisplayNameManager, PrefixProvider {

	private final Map<UUID, String> prefixMap = new HashMap<>();

	@Override
	public void setDisplayName(Player player) {
		setPrefix(player.getUniqueId(),
				StratusAPI.get().getPermissionsManager().getGroupsManager().getPrefixes(player.getUniqueId()));
	}

	@Override
	public String getPrefix(UUID uuid) {
		return prefixMap.getOrDefault(uuid, "");
	}

	@Override
	public void removePlayer(UUID uuid) {
		prefixMap.remove(uuid);
	}

	@Override
	public void setPrefix(UUID uuid, String prefix) {
		Bukkit.getPluginManager().callEvent(new PrefixChangeEvent(uuid, prefixMap.put(uuid, prefix), prefix));
	}

}

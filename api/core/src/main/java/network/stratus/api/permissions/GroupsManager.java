/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.permissions;

import java.util.Collection;
import java.util.SortedSet;
import java.util.UUID;
import javax.annotation.Nullable;

/**
 * Manages permissions {@link Group}s, providing a method of caching the groups
 * locally and obtaining them by name and ID.
 * 
 * @author Ian Ballingall
 *
 */
public interface GroupsManager {

	/**
	 * Get the {@link Group} object for the given unique ID. If the group is not
	 * cached, then null will be returned.
	 * 
	 * @param id The {@link Group}'s unique ID
	 * @return The {@link Group} object
	 */
	@Nullable
	public Group getCachedGroupById(String id);

	/**
	 * Get the {@link Group} object for the given unique name. If the group is not
	 * cached, then null will be returned.
	 * 
	 * @param name The {@link Group}'s unique name
	 * @return The {@link Group} object
	 */
	@Nullable
	public Group getCachedGroupByName(String name);

	/**
	 * Get the {@link Group} object for the given unique ID. It will be obtained
	 * from storage and cached if it is not currently in the cache. <b>This is a
	 * blocking operation.</b>
	 * 
	 * @param id The {@link Group}'s unique ID
	 * @return The {@link Group} object
	 */
	public Group getStoredGroupById(String id);

	/**
	 * Get the {@link Group} object for the given unique name. It will be obtained
	 * from storage and cached if it is not currently in the cache. <b>This is a
	 * blocking operation.</b>
	 * 
	 * @param name The {@link Group}'s unique name
	 * @return The {@link Group} object
	 */
	public Group getStoredGroupByName(String name);

	/**
	 * Add a new {@link Group} to the stored permission groups.
	 * 
	 * @param group The {@link Group} to add
	 */
	public void addGroup(Group group);

	/**
	 * Add a collection of groups to the stored permission groups.
	 * 
	 * @param groups The {@link Collection} of groups to add
	 */
	default public void addGroups(Collection<Group> groups) {
		for (Group g : groups) {
			addGroup(g);
		}
	}

	/**
	 * Remove a {@link Group} from the manager, given its name.
	 * 
	 * @param name The name of the {@link Group}
	 */
	public void removeGroupByName(String name);

	/**
	 * Remove a {@link Group} from the manager, given its ID.
	 * 
	 * @param id The ID of the {@link Group}
	 */
	public void removeGroupById(String id);

	/**
	 * Reset the {@link Group}s stored in the permissions manager. This may be
	 * called, for example, if the permissions are refreshed. <b>The default groups
	 * MUST be re-added after using this method!</b>
	 */
	public void clearGroups();

	/**
	 * Obtains the {@link SortedSet} of {@link Group} objects corresponding to the
	 * given list of group IDs. If the group is not already stored in the manager,
	 * it is retrieved from storage. This may expect the default groups to always be
	 * locally cached and may fail if they are not.<br>
	 * <br>
	 * 
	 * The primary purpose of this method is to get the group objects given a
	 * player's group IDs so that the relevant permissions can be applied.
	 * 
	 * @param groupIds The collection of group IDs to get
	 * @return The {@link SortedSet} of corresponding {@link Group} objects,
	 *         including the default groups
	 */
	public SortedSet<Group> getGroupsFromList(Collection<String> groupIds);

	/**
	 * Add a player as a member of the given {@link Group}. This records their
	 * membership locally, rather than effecting any change in the player's
	 * membership status.
	 * 
	 * @param uuid  The {@link UUID} of the player being added
	 * @param group The {@link Group} to which the player will be added
	 */
	public void addPlayerToGroup(UUID uuid, Group group);

	/**
	 * Adds a player to the given list of {@link Group}s. This records their
	 * membership locally, rather than effecting any change in the player's
	 * membership status.
	 * 
	 * @param uuid   The {@link UUID} of the player being added
	 * @param groups The {@link Group}s to which the player will be added
	 */
	public void addPlayerToGroups(UUID uuid, SortedSet<Group> groups);

	/**
	 * Obtains a list of the player's {@link Group} memberships.
	 * 
	 * @param uuid The {@link UUID} of the player whose {@link Group}s to find
	 * @return The set of the player {@link Group}s
	 */
	public SortedSet<Group> getPlayerGroups(UUID uuid);

	/**
	 * Removes the player from the given {@link Group}. This records their
	 * membership locally, rather than effecting any change in the player's
	 * membership status.
	 * 
	 * @param uuid  The {@link UUID} of the player being removed
	 * @param group The {@link Group} to remove the player from
	 * @return Whether the player was previously a member of this group
	 */
	public boolean removePlayerFromGroup(UUID uuid, Group group);

	/**
	 * Removes all the player's group memberships. This should be done when the
	 * player disconnects from the server.
	 * 
	 * @param uuid The {@link UUID} of the player to remove
	 */
	public void removePlayer(UUID uuid);

	/**
	 * Get this player's prefixes based on their {@link Group}s, in priority
	 * order.
	 * 
	 * @param uuid The {@link UUID} of the player whose prefixes to get
	 * @return The {@link Player}'s prefixes
	 */
	default public String getPrefixes(UUID uuid) {
		SortedSet<Group> groups = getPlayerGroups(uuid);
		if (groups == null)
			return "";

		StringBuilder prefixes = new StringBuilder();
		for (Group g : groups) {
			prefixes.append(g.getPrefix().replace('`', '\u00A7'));
		}

		return prefixes.toString();
	}

}

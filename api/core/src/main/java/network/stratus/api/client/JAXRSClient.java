/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.client;

import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import org.glassfish.jersey.jackson.JacksonFeature;

import network.stratus.api.util.Util;

/**
 * Represents a client which connects to a REST API and performs HTTP requests
 * to retrieve, store and update data using the JAX-RS API.
 * 
 * @author Ian Ballingall
 *
 */
public class JAXRSClient implements APIClient {

	/** The URL of the REST API. */
	private String url;
	/** The internal representation of the REST API client. */
	private Client client;

	/**
	 * Instantiate a new API client with a fixed address.
	 * 
	 * @param url The address of the server where the REST API is hosted
	 * 
	 * @deprecated Use {@link JAXRSClient.Builder}, which allows more extensible
	 *             building of the client. Will be removed in API 7.
	 */
	@Deprecated
	public JAXRSClient(String url) {
		this(url, (String) null);
	}

	/**
	 * Instantiate a new API client with a fixed address.
	 * 
	 * @param url              The address of the server where the REST API is
	 *                         hosted
	 * @param authToken        The authentication token for the REST API
	 * @param exceptionHandler Handles exceptions thrown by this client
	 * 
	 * @deprecated Use {@link JAXRSClient.Builder}, which allows more extensible
	 *             building of the client. Will be removed in API 7.
	 */
	@Deprecated
	public JAXRSClient(String url, String authToken) {
		this.url = url;
		client = ClientBuilder.newClient().register(JacksonFeature.class);

		if (authToken != null) {
			client.register(new AuthTokenFilter(authToken));
		}
	}

	private JAXRSClient(String url, List<?> components) {
		this.url = url;
		client = ClientBuilder.newClient().register(JacksonFeature.class);
		components.forEach(client::register);
	}

	public String getUrl() {
		return url;
	}

	@Override
	@Deprecated
	public <T> APIResponse<T> get(APIRequest<T> request) {
		return createTarget(request).request(MediaType.APPLICATION_JSON).get(
				new GenericType<APIResponse<T>>(Util.getGenericType(APIResponse.class, request.getResponseType())));
	}

	@Override
	@Deprecated
	public <T> APIResponse<T> post(APIRequest<T> request) {
		return createTarget(request).request(MediaType.APPLICATION_JSON).post(Entity.json(request),
				new GenericType<APIResponse<T>>(Util.getGenericType(APIResponse.class, request.getResponseType())));
	}

	@Override
	@Deprecated
	public <T> APIResponse<T> put(APIRequest<T> request) {
		return createTarget(request).request(MediaType.APPLICATION_JSON).put(Entity.json(request),
				new GenericType<APIResponse<T>>(Util.getGenericType(APIResponse.class, request.getResponseType())));
	}

	@Override
	@Deprecated
	public <T> APIResponse<T> delete(APIRequest<T> request) {
		return createTarget(request).request(MediaType.APPLICATION_JSON).delete(
				new GenericType<APIResponse<T>>(Util.getGenericType(APIResponse.class, request.getResponseType())));
	}

	/**
	 * Instantiates a WebTarget instance to the endpoint with query parameters.
	 * 
	 * @param <T>     The type of object to be returned by this request
	 * @param request The request object
	 * @return The target object
	 */
	@Deprecated
	private <T> WebTarget createTarget(APIRequest<T> request) {
		WebTarget target = client.target(url + request.getEndpoint());
		for (Entry<String, Object> entry : request.getProperties().entrySet()) {
			target = target.queryParam(entry.getKey(), entry.getValue());
		}

		return target;
	}

	@Override
	public <T> T get(Request<T> request) throws RequestFailureException {
		try {
			Invocation.Builder builder = createTarget(request).request(MediaType.APPLICATION_JSON);
			request.getHeaders().forEach((key, value) -> builder.header(key, value));
			return builder.get(request.getResponseType());
		} catch (WebApplicationException e) {
			processException(e);
		} catch (ProcessingException e) {
			processException(e);
		}

		return null;
	}

	@Override
	public <T> T post(Request<T> request) throws RequestFailureException {
		try {
			Invocation.Builder builder = createTarget(request).request(MediaType.APPLICATION_JSON);
			request.getHeaders().forEach((key, value) -> builder.header(key, value));
			return builder.post(Entity.json(request), request.getResponseType());
		} catch (WebApplicationException e) {
			processException(e);
		} catch (ProcessingException e) {
			processException(e);
		}

		return null;
	}

	@Override
	public <T> T put(Request<T> request) throws RequestFailureException {
		try {
			Invocation.Builder builder = createTarget(request).request(MediaType.APPLICATION_JSON);
			request.getHeaders().forEach((key, value) -> builder.header(key, value));
			return builder.put(Entity.json(request), request.getResponseType());
		} catch (WebApplicationException e) {
			processException(e);
		} catch (ProcessingException e) {
			processException(e);
		}

		return null;
	}

	@Override
	public <T> T delete(Request<T> request) throws RequestFailureException {
		try {
			Invocation.Builder builder = createTarget(request).request(MediaType.APPLICATION_JSON);
			request.getHeaders().forEach((key, value) -> builder.header(key, value));
			return builder.delete(request.getResponseType());
		} catch (WebApplicationException e) {
			processException(e);
		} catch (ProcessingException e) {
			processException(e);
		}

		return null;
	}

	/**
	 * Instantiates a WebTarget instance to the endpoint with query parameters.
	 * 
	 * @param <T>     The type of object to be returned by this request
	 * @param request The request object
	 * @return The target object
	 */
	private <T> WebTarget createTarget(Request<T> request) {
		WebTarget target = client.target(url + request.getEndpoint());
		for (Entry<String, Object> entry : request.getProperties().entrySet()) {
			target = target.queryParam(entry.getKey(), entry.getValue());
		}

		return target;
	}

	/**
	 * Processes the given {@link WebApplicationException}, producing a
	 * {@link RequestFailureException} containing the {@link ErrorResponse}.
	 */
	private void processException(WebApplicationException e) throws RequestFailureException {
		throw new RequestFailureException("Failed to perform request", e, ErrorResponse.parse(e.getResponse()));
	}

	/**
	 * Processes the given {@link ProcessingException}, producing a
	 * {@link RequestFailureException}.
	 */
	private void processException(ProcessingException e) throws RequestFailureException {
		throw new RequestFailureException("Failed to perform request", e);
	}

	/**
	 * Builds a new {@link JAXRSClient} with the given settings.
	 */
	public static class Builder {

		private String url;
		private MultivaluedHashMap<String, ? super Object> headers = new MultivaluedHashMap<>();
		private LinkedList<? super Object> components = new LinkedList<>();

		/**
		 * Create a new builder. The URL is required.
		 * 
		 * @param url The API URL, without trailing slash
		 */
		public Builder(String url) {
			this.url = url;
		}

		/**
		 * Register the API authentication token. Null or empty tokens are ignored.
		 * 
		 * @param authToken The authentication token
		 * @return The builder
		 */
		public Builder authToken(String authToken) {
			if (authToken != null && !authToken.isEmpty())
				headers.putSingle("X-Auth-Token", authToken);
			return this;
		}

		/**
		 * Register a single-valued header field for all requests.
		 * 
		 * @param key   The header key
		 * @param value The header value
		 * @return The builder
		 */
		public Builder header(String key, Object value) {
			headers.putSingle(key, value);
			return this;
		}

		/**
		 * Register a multi-valued header field for all requests.
		 * 
		 * @param key    The header key
		 * @param values The list of values
		 * @return The builder
		 */
		public Builder header(String key, List<Object> values) {
			headers.put(key, values);
			return this;
		}

		/**
		 * Register a custom component that will be registered with the {@link Client}.
		 * These are, for example, request and response filters.
		 * 
		 * @param filter The custom component
		 * @return The builder
		 */
		public Builder component(Object filter) {
			components.add(filter);
			return this;
		}

		/**
		 * Obtain the corresponding {@link JAXRSClient}.
		 * 
		 * @return The client
		 */
		public JAXRSClient build() {
			components.addFirst(new HeaderFilter(headers));
			return new JAXRSClient(url, components);
		}

	}

}

/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.client;

import java.io.IOException;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;

/**
 * Filters requests made by the API client and inserts the authentication token.
 * 
 * @author Ian Ballingall
 *
 * @deprecated Use a {@link HeaderFilter} with {@code X-Auth-Token} key
 *             containing the auth token value. Will be removed in API 7.
 */
@Deprecated
public class AuthTokenFilter implements ClientRequestFilter {

	/** The authentication token for the REST API. */
	private String authToken;

	/**
	 * Create a new instance of this filter with the given token.
	 * 
	 * @param authToken The authentication token for this filter
	 */
	public AuthTokenFilter(String authToken) {
		this.authToken = authToken;
	}

	@Override
	public void filter(ClientRequestContext requestContext) throws IOException {
		requestContext.getHeaders().putSingle("X-Auth-Token", authToken);
	}

}

/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.client;

/**
 * Represents a response received from the API to a request.
 * 
 * @author Ian Ballingall
 *
 * @param <T> The type of object included in the response
 * 
 * @deprecated This should no longer be used as it is redundant extra data and
 *             bad practice. Instead, the response will be returned directly.
 *             This will be removed in API 7.
 */
@Deprecated
public class APIResponse<T> {

	/** The response status returned by the API. This is not the HTTP status code. */
	private ResponseStatus status;
	/** The description of the response returned by the API. */
	private String description;
	/** The object containing relevant data from the response. */
	private T responseObject;

	/**
	 * Get the response status returned by the API. This is not the HTTP status
	 * code.
	 * 
	 * @return The response status
	 */
	public ResponseStatus getStatus() {
		return status;
	}

	/**
	 * Get the description of the response returned by the API. This may provide
	 * additional information about what happened with the request, particularly for
	 * a failure status.
	 * 
	 * @return The response description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Get the deserialised object which was returned for the request. This object
	 * contains the relevant data from the response.
	 * 
	 * @return The object returned for the request
	 */
	public T getResponseObject() {
		return responseObject;
	}

}

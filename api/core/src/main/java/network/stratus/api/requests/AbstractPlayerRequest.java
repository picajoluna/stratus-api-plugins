/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.requests;

import java.util.UUID;

/**
 * Represents a type of request to the server for player data. This is designed
 * to provide a set of common fields for requests for player data, but does not
 * correspond to a request itself.
 * 
 * @author Ian Ballingall
 *
 */
public abstract class AbstractPlayerRequest {

	private UUID uuid;
	private String username;
	private String ip;

	public AbstractPlayerRequest(UUID uuid, String username, String ip) {
		this.uuid = uuid;
		this.username = username;
		this.ip = ip;
	}

	public UUID getUuid() {
		return uuid;
	}

	public String getUsername() {
		return username;
	}

	public String getIp() {
		return ip;
	}

}

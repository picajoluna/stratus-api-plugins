/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.models.punishment;

import java.util.Date;
import java.util.UUID;

import network.stratus.api.models.punishment.PunishmentFactory.Type;

/**
 * Abstract representation of an {@link Enforceable} punishment. Subclasses must
 * define how the punishment is to be enforced.
 * 
 * @author Ian Ballingall
 *
 */
public abstract class Punishment implements Enforceable {

	protected String _id;
	protected UUID issuer;
	protected String issuerOfflineName;
	protected UUID target;
	protected String targetOfflineName;
	protected Date time;
	protected Date expiry;
	protected String reason;
	protected boolean active;
	protected int number;
	protected String serverName;

	public Punishment(String _id, UUID issuer, String issuerOfflineName, UUID target, String targetOfflineName,
			Date time, Date expiry, String reason, boolean active, int number, String serverName) {
		this._id = _id;
		this.issuer = issuer;
		this.issuerOfflineName = issuerOfflineName;
		this.target = target;
		this.targetOfflineName = targetOfflineName;
		this.time = time;
		this.expiry = expiry;
		this.reason = reason;
		this.active = active;
		this.number = number;
		this.serverName = serverName;
	}

	public String get_id() {
		return _id;
	}

	public UUID getIssuer() {
		return issuer;
	}

	public String getIssuerOfflineName() {
		return issuerOfflineName;
	}

	public UUID getTarget() {
		return target;
	}

	public String getTargetOfflineName() {
		return targetOfflineName;
	}

	public Date getTime() {
		return time;
	}

	public Date getExpiry() {
		return expiry;
	}

	public String getReason() {
		return reason;
	}

	public boolean isActive() {
		return active;
	}

	public int getNumber() {
		return number;
	}

	public String getServerName() {
		return serverName;
	}

	/**
	 * Determines whether the punishment has expired.
	 * 
	 * @return Whether the punishment has expired
	 */
	public boolean hasExpired() {
		return expiry != null && expiry.compareTo(new Date()) < 0;
	}

	/**
	 * Obtain the {@link Type} representation of this punishment.
	 * 
	 * @return The {@link Type} of this punishment
	 */
	public abstract Type getType();

	/**
	 * Initialises the player objects corresponding to the issuer and target, if
	 * available to the implementation.
	 * 
	 * @return This punishment
	 */
	public abstract Punishment initialisePlayers();

	/**
	 * Formats the usernames corresponding to the issuer and target, as defined for
	 * offline players by the implementation.
	 * 
	 * @return This punishment
	 */
	public abstract Punishment formatNames();

}

/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.client;

/**
 * Represents the response status of an API request. This is not the same as the
 * HTTP status code. These codes will only be available if a successful HTTP
 * status code is returned.
 * 
 * @author Ian Ballingall
 * 
 * @deprecated Pertains to the old request/response structure. This will be
 *             removed in API 7.
 */
@Deprecated
public enum ResponseStatus {
	/** Indicates the activity was successful. */
	SUCCESS(0),
	/** Indicates an unspecified failure occurring during the operation. */
	FAILURE_GENERIC(1);

	private int value;

	private ResponseStatus(int value) {
		this.value = value;
	}

	/**
	 * Get the integer representation of this status.
	 * 
	 * @return The integer representation of this status
	 */
	public int getValue() {
		return value;
	}
}

/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.client;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Represents a request to be made to a REST API.
 * 
 * @author Ian Ballingall
 *
 * @param <T> The type of object to be returned by the API
 * 
 * @deprecated This relies on the older structure for API requests and should no
 *             longer be used. This will be removed in API 7.
 */
@Deprecated
public interface APIRequest<T> {

	/**
	 * Get the API endpoint for this request.
	 * 
	 * @return The API endpoint
	 */
	@JsonIgnore
	public String getEndpoint();

	/**
	 * Get the type of object returned by this API call.
	 * 
	 * @return The class of object returned by call
	 */
	@JsonIgnore
	public Class<T> getResponseType();

	/**
	 * Get the query parameters for this request. This will return an empty
	 * {@link Map} if not otherwise defined by the implementing class.
	 * 
	 * @return A {@link Map} containing query parameters
	 */
	@JsonIgnore
	default public Map<String, Object> getProperties() {
		return new HashMap<String, Object>();
	}

	/**
	 * Make the request to the API.
	 * 
	 * @param client The client to use for the request
	 * @return The response object for this call
	 */
	@JsonIgnore
	public APIResponse<T> makeRequest(APIClient client);

}

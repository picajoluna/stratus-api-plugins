/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2020 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.models.punishment;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Abstract representation of a factory which manufactures the appropriate type
 * of {@link Punishment}, which can then be enforced.
 * 
 * @author Ian Ballingall
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class PunishmentFactory {

	public PunishmentFactory() {
	}

	public PunishmentFactory(String _id, UUID issuer, String issuerOfflineName, UUID target, String targetOfflineName,
			Type type, Date time, Date expiry, String reason, boolean active, int number, long timeRemaining,
			String serverName) {
		this._id = _id;
		this.issuer = issuer;
		this.issuerOfflineName = issuerOfflineName;
		this.target = target;
		this.targetOfflineName = targetOfflineName;
		this.type = type;
		this.time = time;
		this.expiry = expiry;
		this.reason = reason;
		this.active = active;
		this.number = number;
		this.timeRemaining = timeRemaining;
		this.serverName = serverName;
	}

	/** The unique ID of this punishment. */
	protected String _id;
	/** The UUID of the punishment issuer. */
	protected UUID issuer;
	/** The offline name of the issuer. */
	protected String issuerOfflineName;
	/** The UUID of the punishment target. */
	protected UUID target;
	/** The offline name of the target. */
	protected String targetOfflineName;
	/** The type of punishment. */
	protected Type type;
	/** The time of issue. */
	protected Date time;
	/** The expiry time. */
	protected Date expiry;
	/** The reason for the punishment. */
	protected String reason;
	/** Whether this punishment is active. */
	protected boolean active;
	/** The number of this punishment for this player. */
	protected int number;
	/** The time remaining for this punishment, if it is a timed punishment. */
	protected long timeRemaining;
	/** The name of the server on which this punishment was issued. */
	protected String serverName;

	/**
	 * Represents the type of a punishment.
	 */
	public enum Type {
		WARN("WARN"), KICK("KICK"), BAN("BAN"), MUTE("MUTE"),
		/**
		 * Used to denote that the punishment type should be determined automatically.
		 */
		AUTO("AUTO");

		private String type;
		private static final Set<Type> TIMED_TYPES = Collections
				.unmodifiableSet(new HashSet<>(Arrays.asList(new Type[] { MUTE })));

		private Type(String type) {
			this.type = type;
		}

		/**
		 * Get the string representation of this punishment type.
		 * 
		 * @return The string representation of this punishment type
		 */
		public String getType() {
			return type;
		}

		/**
		 * Whether this type corresponds to a timed punishment.
		 * 
		 * @return If this type is timed
		 */
		public boolean isTimedType() {
			return TIMED_TYPES.contains(this);
		}
	}

	public String get_id() {
		return _id;
	}

	public UUID getIssuer() {
		return issuer;
	}

	public String getIssuerOfflineName() {
		return issuerOfflineName;
	}

	public UUID getTarget() {
		return target;
	}

	public String getTargetOfflineName() {
		return targetOfflineName;
	}

	public Type getType() {
		return type;
	}

	public Date getTime() {
		return time;
	}

	public Date getExpiry() {
		return expiry;
	}

	public String getReason() {
		return reason;
	}

	public boolean isActive() {
		return active;
	}

	public int getNumber() {
		return number;
	}

	public String getServerName() {
		return serverName;
	}

	public long getTimeRemaining() {
		return timeRemaining;
	}

	/**
	 * Obtain the {@link Punishment} corresponding to the configured fields.
	 * 
	 * @return The punishment of the appropriate type
	 */
	public abstract Punishment getObject();

}
